# Makefile
#
#
ENV := dev

server:
	php app/console gps:server --env=$(ENV)

zombie:
	php app/console gps:zombie --env=$(ENV)

cc:
	php app/console cache:clear --env=$(ENV)

warmup:
	php app/console cache:warmup --env=$(ENV)

rmlogs:
	rm app/logs/apache.log app/logs/access.log
	touch app/logs/apache.log app/logs/access.log

database:
	php app/console doctrine:database:drop --force --env=$(ENV)
	php app/console doctrine:database:create --env=$(ENV)
	php app/console doctrine:schema:create --env=$(ENV)

dbupdate:
	php app/console doctrine:schema:update --force --env=$(ENV)

devusers:
	php app/console fos:user:create admin admin@localhost admin
	php app/console fos:user:promote admin ROLE_ADMIN
	php app/console fos:user:create user user@localhost user
	php app/console fos:user:promote user ROLE_USER

fixtures:
	php app/console doctrine:fixtures:load --env=$(ENV)

assets:
	app/console assets:install web --symlink --relative --env=$(ENV)
	app/console assetic:dump --env=$(ENV)

permissions:
	setfacl -m default:group:www-data:rwX web/uploads/
	setfacl -m default:user:$(USER):rwX web/uploads/
	setfacl -m group:www-data:rwX web/uploads/
	setfacl -m user:$(USER):rwX web/uploads/
	setfacl -m default:group:www-data:rwX app/cache/
	setfacl -m default:user:$(USER):rwX app/cache/
	setfacl -m group:www-data:rwX app/cache/
	setfacl -m user:$(USER):rwX app/cache/
	setfacl -m default:group:www-data:rwX app/logs/
	setfacl -m default:user:$(USER):rwX app/logs/
	setfacl -m group:www-data:rwX app/logs/
	setfacl -m user:$(USER):rwX app/logs/
	rm -rf app/cache/* app/logs/* web/uploads/*

prod:
	ENV=prod make cc -e
	ENV=prod make warmup -e
	ENV=prod make assets -e
#	ENV=prod make htaccess -e
#	chown apache:apache -R app/cache app/logs

