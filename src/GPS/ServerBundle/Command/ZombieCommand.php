<?php

namespace GPS\ServerBundle\Command;

use GPS\ServerBundle\Simulator\MeitrackVT310;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ZombieCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setDefinition([
                new InputArgument('direccion', InputArgument::OPTIONAL, 'Dirección del servidor', 'localhost:8080'),
                new InputOption('trackers', null, InputOption::VALUE_OPTIONAL, 'Numero de Trackers que seran simulados', 10),
                new InputOption('cantidad', null, InputOption::VALUE_OPTIONAL, 'Numero de tramas que seran enviadas por cada Tracker', 10),
                new InputOption('intervalo', null, InputOption::VALUE_OPTIONAL, 'Intervalo de tiempo en el que se envian las tramas', 0.5),
            ])
            ->setName('gps:zombie')
            ->setDescription('Simula GPS Trackers')
            ->setHelp(<<<EOF
El comando <info>%command.name%</info> simula un Tracker enviando una trama al servidor:

  <info>%command.full_name%</info>

Para cambiar dirección del servidor use el argumento <info>direccion</info>:

  <info>%command.full_name% localhost:8888</info>

Para cambiar el numero de trackers use la opción <info>trackers</info>:

  <info>%command.full_name% --trackers=5</info>

Para la cantidad de tramas enviadas por cada tracker use la opción <info>cantidad</info>:

  <info>%command.full_name% --cantidad=10</info>

Para cambiar el intervalo de tiempo en segundos entre cada trama use la opción <info>intervalo</info>:

  <info>%command.full_name% --intervalo=30</info>
EOF
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $address = $input->getArgument('direccion');
        $trackers = $input->getOption('trackers');
        $cantidad = $input->getOption('cantidad');
        $intervalo = $input->getOption('intervalo');

        // Open the doors!
        try {
            $fp = stream_socket_client(sprintf('tcp://%s', $address), $errno, $errstr, 30, STREAM_CLIENT_ASYNC_CONNECT);
        } catch (\Exception $e) {
            $output->writeln('<error>Error en la conexión</error>');
            $output->writeln(sprintf('%s => %s (%s)', $address, $e->getMessage(), $e->getCode()));
            return 1;
        }

        $output->writeln(sprintf('Zombie escribiendo en <info>%s</info>', $address));

        // Invoke zombies in Arequipa
        $southWest = array(-16.3749, -71.5675);
        $northEast = array(-16.3989, -71.5369);

        $latSpan = $northEast[0] - $southWest[0];
        $lngSpan = $northEast[1] - $southWest[1];

        $zombies = array();
        for ($i = 0; $i < $trackers; $i++) {
            $zombies[$i]['latitude'] = $southWest[0] + $latSpan * rand(1, 99) / 100;    // standard latitude gg.mmss
            $zombies[$i]['longitude'] = $southWest[1] + $lngSpan * rand(1, 99) / 100;   // standard longitude ggg.mmss
            $zombies[$i]['angle'] = rand(1, 36000) / 100;
        }

        // Run zombies run!!!
        for ($i = 0; $i < $cantidad; $i++) {
            for ($j = 0; $j < $trackers; $j++) {
                $output->writeln(sprintf('<comment>#%s</comment> (<info>%s</info>/%s)', $j, $i + 1, $cantidad));

                // randomize movement
                $zombies[$j]['angle'] = ($zombies[$j]['angle'] - (rand(-4000, 4000) / 100)) % 360.00;
                $zombies[$j]['speed'] = rand(5000, 7000) / 100;
                $datetime = new \DateTime();

                // set data
                $zombies[$j]['latitude'] += ($zombies[$j]['speed'] * 0.00001 * sin(deg2rad($zombies[$j]['angle'])));
                $zombies[$j]['longitude'] += ($zombies[$j]['speed'] * 0.00001 * cos(deg2rad($zombies[$j]['angle'])));

                // prepare plot
                $plot = MeitrackVT310::makePlot($j,
                    $zombies[$j]['latitude'],
                    $zombies[$j]['longitude'],
                    $zombies[$j]['speed'],
                    $zombies[$j]['angle'],
                    $datetime);

                // write plot
                try {
                    fwrite($fp, $plot . "\0");
                    $output->writeln(sprintf('<info>-</info> %s', $plot));
                } catch (\Exception $e) {
                    fclose($fp);
                    $output->writeln(sprintf('<error>Se perdió la conexión con el servidor %s</error>', $address));
                    return 1;
                }

                usleep($intervalo * 1000000);
            }
        }
        fclose($fp);
        return 0;
    }
}