<?php

namespace GPS\ServerBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Ratchet\MessageComponentInterface;
use Ratchet\Server\IoServer;

class ServerRunCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setDefinition(array(
                new InputArgument('port', InputArgument::OPTIONAL, 'The port to be listening', 8080),
            ))
            ->setName('gps:server')
            ->setDescription('Start the GPS server')
            ->setHelp(<<<EOF
The <info>%command.name%</info> runs GPS parser server:

  <info>%command.full_name%</info>

To change default bind port use the <info>port</info> argument:

  <info>%command.full_name% 8080</info>
EOF
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(sprintf("Server running on <info>localhost:%s</info>\n", $input->getArgument('port')));

        /** @var MessageComponentInterface $gps */
        $gps = $this->getContainer()->get('gps_server.listener');
        $server = IoServer::factory($gps, $input->getArgument('port'));
        $server->run();
    }
}