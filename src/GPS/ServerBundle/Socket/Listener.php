<?php

namespace GPS\ServerBundle\Socket;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use GPS\ServerBundle\Driver\DriverInterface;
use GPS\TrackBundle\Entity\Tracker;
use GPS\TrackBundle\Entity\Trama;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

class Listener implements MessageComponentInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var DriverInterface[]
     */
    protected $drivers;

    /**
     * @var ConsoleOutput
     */
    protected $output;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;

        $this->output = new ConsoleOutput();
        $this->drivers = [];
    }

    public function addDriver($driver)
    {
        if (!in_array($driver, $this->drivers))
        {
            $this->drivers[] = $driver;
        }
    }

    public function onOpen(ConnectionInterface $conn)
    {
    }

    public function onClose(ConnectionInterface $conn)
    {
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->output->writeln($e->getMessage());
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        /** @var $driver DriverInterface */
        /** @var Tracker $tracker */

        $hexmsg = bin2hex($msg);
        $this->output->writeln(sprintf("\n%s", $msg));
        foreach ($this->drivers as $driver)
        {
            if ($datos = $driver->match($hexmsg))
            {
                $hexid = hex2bin($datos['id']);
                $id = "";

                for ($i = 0; $i < 7; $i++) {
                        $b = $hexid[$i];

                    // First digit
                    $d1 = ($b & 0xf0) >> 4;
                    if ($d1 == 0xf) break;
                    $id += $d1;

                    // Second digit
                    $d2 = ($b & 0x0f);
                    if ($d2 == 0xf) break;
                    $id += $d2;
                }

                if ($tracker = $this->getRepository()->findOneBy(['codigo' => $id]))
                {
                    $trama = new Trama();
                    $driver->parse($datos, $trama);

                    $trama->setMessage($hexmsg);
                    $trama->setDriver($driver->getName());
                    $trama->setTracker($tracker);

                    $this->em->persist($trama);
                    $this->em->flush();

                    $this->output->writeln(sprintf("trama %s registrada", $trama->getId()));
                }
                else
                {
                    $this->output->writeln(sprintf("tracker %s no encontrado", $datos['id']));
                }
            }
        }
    }

    /**
     * @return ObjectRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('GPS\TrackBundle\Entity\Tracker');
    }
}