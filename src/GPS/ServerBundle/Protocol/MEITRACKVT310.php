<?php

namespace GPS\ServerBundle\Driver;

use GPS\TrackBundle\Entity\Trama;

class MEITRACKVT310
{
    public function parse($datos, Trama $trama)
    {
        $trama->setTimestamp(\DateTime::createFromFormat('His.u', hex2bin($datos['time'])));
        $trama->setValid(hex2bin($datos['status']));
        $trama->setLatitude((float) hex2bin($datos['latitude']) / 100);
        if (hex2bin($datos['laorientation']) == 'S')
            $trama->setLatitude($trama->getLatitude() * -1);
        $trama->setLongitude((float) hex2bin($datos['longitude']) / 100);
        if (hex2bin($datos['laorientation']) == 'S')
            $trama->setLongitude($trama->getLongitude() * -1);
        $trama->setSpeed((float)hex2bin($datos['speed']));
        $trama->setAngle((float)hex2bin($datos['heading']));
        $trama->setDatestamp(\DateTime::createFromFormat('dmy', hex2bin($datos['date'])));
        $trama->setChecksum(hex2bin($datos['checksum']));

        /**
         * TODO: hay un problema con la codificacion de caracteres, si el juego de caracteres es invalido la trama se pierde.
         * TODO: revisar si es problema del zombie que envia mal las tramas o si es un problema de la codificacion multibyte
         */
        try {
            $sensors = hex2bin($datos['sensors']);
            $sensors = iconv('UTF-8', 'ASCII//TRANSLIT', utf8_encode($sensors));
            $sensors = trim($sensors, '|');
            $sensors = explode('|', $sensors);
            $trama->setSensors($sensors);
        } catch (\Exception $e) {
            $trama->setSensors([]);
        }
    }

    public function getPattern()
    {
        return '/^2424007.(?<id>[\dA-Fa-f]{1,14})9955(?<time>[\dA-Fa-f]{12}2e[\dA-Fa-f]{6})2c(?<status>41|56)2c(?<latitude>[\dA-Fa-f]{8}2e[\dA-Fa-f]{8})2c(?<laorientation>53|4E)2c(?<longitude>[\dA-Fa-f]{10}2e[\dA-Fa-f]{8})2c(?<loorientation>57|45)2c(?<speed>[\dA-Fa-f]{2,6}2e[\dA-Fa-f]{0,4})2c(?<heading>[\dA-Fa-f]{2,6}(2e[\dA-Fa-f]{0,4})?)2c(?<date>[\dA-Fa-f]{12})2c(?<magvariation>[\dA-Fa-f]{2,6}2e[\dA-Fa-f]{2,4})?2c(?<magorientation>57|45)?2a(?<checksum>[\dA-Fa-f]{4})7c(?<sensors>.*)/';
    }

    public function getName()
    {
        return 'Meitrack VT310';
    }
}
