<?php

namespace GPS\ServerBundle\Simulator;


class MeitrackVT310
{
    static public function makePlot($id, $latitude, $longitude, $speed, $angle, \Datetime $datetime)
    {
        // http://vss.pl/traker/gprs.pdf
        $data = sprintf('%s,%s,%s,%s,%s,%s,%s,%s,%s', // data  -> 0x9955 Single location report
            sprintf('%s.000', $datetime->format('His')), // time hhmmss.ddd
            rand(0, 100) ? 'A' : 'V', // 1% probability of invalid
            sprintf('%09s', number_format(abs($latitude) * 100, 4, '.', '')), // change xx.xxxx to xxxx.xxxx
            $latitude < 0 ? 'S' : 'N', // set N = North, S = South
            sprintf('%010s', number_format(abs($longitude) * 100, 4, '.', '')), // change xx.xxxx to xxxx.xxxx
            $latitude < 0 ? 'W' : 'E', // E = East, W = West
            number_format($speed, 2, '.', ''), // speed xxx.x
            number_format($angle, 2, '.', ''), // angle xxx.x
            $datetime->format('dmy') // date ddmmyy
        );

        $plot = sprintf('%s%s%s%s%s',
            sprintf('%c%c', 36, 36), // $$       -> header
            sprintf('%c%c', 00, 126), // L        -> length of the whole packet
            sprintf("%'f-7s", dechex($id)), // ID       -> Tracker ID
            sprintf('%c%c', 153, 85), // command  -> 0x9955 Single location report
            $data
        );

        // set checksum
        $hexPlot = sprintf("%c", $plot);

        // calculate CRC-CCITT
        // https://forums.digitalpoint.com/threads/php-define-function-calculate-crc-16-ccitt.2584389/
        $crc = 0xFFFF;
        for ($i = 0; $i < strlen($hexPlot); $i++) {
            $x = (($crc >> 8) ^ ord($hexPlot[$i])) & 0xFF;
            $x ^= $x >> 4;
            $crc = (($crc << 8) ^ ($x << 12) ^ ($x << 5) ^ $x) & 0xFFFF;
        }



        $crc = 98;
        $sensors = '1.0|2462|0000|0006,0006|02CC0006151AC5C8|0E|00001BEE';
        // complete plot
        $plot = sprintf('%s,,*%s|%s\n', $plot, $crc, $sensors);

        return $plot;
    }
}