<?php

namespace GPS\ServerBundle\Driver;

abstract class AbstractDriver implements DriverInterface
{
    public function match($mensaje)
    {
        $pattern = $this->getPattern();
        preg_match($pattern, $mensaje, $matches);
        return ($matches == array()) ? false : $matches;
    }
}
