<?php

namespace GPS\ServerBundle\Driver;

use GPS\TrackBundle\Entity\Trama;

interface DriverInterface
{
    public function parse($datos, Trama $trama);

    public function match($mensaje);

    public function getPattern();

    public function getName();
}
