<?php

namespace GPS\ServerBundle\Driver;

use GPS\TrackBundle\Entity\Trama;

class MEITRACKVT340 extends AbstractDriver
{
    public function parse($datos, Trama $trama)
    {
        $trama->setTimestamp(\DateTime::createFromFormat('His.u', (hex2bin($datos['time']) . '.000')));
        $trama->setValid( hex2bin($datos['status']));
        $trama->setLatitude( (float) hex2bin($datos['latitude']));
        $trama->setLongitude( (float) hex2bin($datos['longitude']));
        $trama->setSpeed( (float) hex2bin($datos['speed']));
        $trama->setAngle( (float) hex2bin($datos['heading']));
        $trama->setDatestamp(\DateTime::createFromFormat('dmy', hex2bin($datos['date'])));
        $trama->setChecksum( hex2bin($datos['checksum']));

        $sensors = hex2bin($datos['sensors']);
        $sensors = explode('|', mb_substr(trim($sensors, '|'), 0, 1));
        $trama->setSensors($sensors);
    }

    public function getPattern()
    {
        return '/^2424(?<flag>[\dA-Fa-f]{2})(?<l>[\dA-Fa-f]{2,6})2c(?<id>[\dA-Fa-f]{30})2c4141412c(?<code>[\dA-Fa-f]{2,4})2c(?<latitude>(2d)?[\dA-Fa-f]{2,4}2e[\dA-Fa-f]{12})2c(?<longitude>(2d)?[\dA-Fa-f]{2,6}2e[\dA-Fa-f]{12})2c(?<date>[\dA-Fa-f]{12})(?<time>[\dA-Fa-f]{12})2c(?<status>41|56)2c(?<nsatellites>[\dA-Fa-f]{2,4})2c(?<gsignal>[\dA-Fa-f]{2,4})2c(?<speed>[\dA-Fa-f]{2,6})2c(?<heading>[\dA-Fa-f]{2,6})2c(?<hdop>[\dA-Fa-f]{2,4}2e[\dA-Fa-f]{2})2c(?<altitude>[\dA-Fa-f]{2,8})2c(?<mileage>[\dA-Fa-f]{2,20})2c(?<runtime>[\dA-Fa-f]{2,20})2c(?<sensors>.*)2c2a(?<checksum>[\dA-Fa-f]{4})0d0a/';
    }

    public function getName()
    {
        return 'Meitrack VT340';
    }

}
