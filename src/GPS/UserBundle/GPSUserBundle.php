<?php

namespace GPS\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class GPSUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
