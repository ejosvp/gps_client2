<?php

namespace GPS\UserBundle\Doctrine;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use FOS\UserBundle\Model\GroupInterface;
use FOS\UserBundle\Doctrine\GroupManager as BaseGroupManager;
use GPS\TrackBundle\Security\ClientContext;
use Symfony\Component\Security\Core\SecurityContextInterface;

class GroupManager extends BaseGroupManager
{
    /** @var  ClientContext */
    protected $clientConext;

    /** @var  SecurityContextInterface */
    protected $securityContext;

    public function __construct(ObjectManager $om, ClientContext $clientContext,
                                SecurityContextInterface $securityContext, $class)
    {
        parent::__construct($om, $class);

        $this->clientContext = $clientContext;
        $this->securityContext = $securityContext;
    }

    public function createGroup($name)
    {
        $class = $this->getClass();
        $group = new $class($name);
        if ($this->getClient())
        {
            $group->setCliente($this->getClient());
        }

        return $group;
    }

    public function deleteGroup(GroupInterface $group)
    {
        $this->objectManager->remove($group);
        $this->objectManager->flush();
    }

    public function findGroupBy(array $criteria)
    {
        $criteria = array_merge($criteria, array('cliente' => $this->getClient()));
        return $this->repository->findOneBy($criteria);
    }

    public function findGroups()
    {
        return $this->repository->findBy(array('cliente' => $this->getClient()));
    }

    public function findParents()
    {
        return $this->getQueryBuilder()
            ->andWhere('g.parent IS NULL')
            ->getQuery()->execute();
    }

    public function updateGroup(GroupInterface $group, $andFlush = true)
    {
        parent::updateGroup($group, $andFlush);
    }

    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->getRepository()->createQueryBuilder('g')
            ->where('g.cliente = :client')
            ->setParameters(array(
                'client' => $this->getClient(),
            ));
    }

    /**
     * @return QueryBuilder
     */
    public function getCurrentUserGroupsQueryBuilder()
    {
        return $this->getRepository()->createQueryBuilder('g')
            ->leftJoin('g.parent', 'p')
            ->where('g.cliente = :client')
            ->andWhere(':user MEMBER OF g.usuarios OR :user MEMBER OF p.usuarios')
            ->setParameters(array(
                'client' => $this->getClient(),
                'user' => $this->getUser(),
            ));
    }

    public function getUser()
    {
        return $this->securityContext->getToken()->getUser();
    }

    public function getClient()
    {
        return $this->clientContext->getCurrent();
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }
}
