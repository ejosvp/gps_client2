<?php

namespace GPS\UserBundle\Doctrine;

use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Doctrine\UserManager as BaseUserManager;
use FOS\UserBundle\Util\CanonicalizerInterface;
use GPS\TrackBundle\Security\ClientContext;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class UserManager extends BaseUserManager
{
    /** @var  ClientContext */
    protected $clientContext;

    public function __construct(EncoderFactoryInterface $encoderFactory, CanonicalizerInterface $usernameCanonicalizer, CanonicalizerInterface $emailCanonicalizer, ObjectManager $om, ClientContext $clientContext, $class)
    {
        parent::__construct($encoderFactory, $usernameCanonicalizer, $emailCanonicalizer, $om, $class);

        $this->clientContext = $clientContext;
    }

    public function createUser()
    {
        $class = $this->getClass();
        $user = new $class;
        if ($this->getClient())
        {
            $user->setCliente($this->getClient());
        }

        return $user;
    }

    public function deleteUser(UserInterface $user)
    {
        $this->objectManager->remove($user);
        $this->objectManager->flush();
    }

    public function findUserBy(array $criteria)
    {
        $criteria = array_merge($criteria, ['cliente' => $this->getClient()]);
        return $this->repository->findOneBy($criteria);
    }

    public function findUsers()
    {
        return $this->repository->findBy(['cliente' => $this->getClient()]);
    }

    public function reloadUser(UserInterface $user)
    {
        $this->objectManager->refresh($user);
    }

    public function updateUser(UserInterface $user, $andFlush = true)
    {
        parent::updateUser($user, $andFlush);
    }

    public function getQueryBuilder()
    {
        return $this->repository->createQueryBuilder('u')
            ->where('u.cliente = :cliente')
            ->setParameter('cliente', $this->getClient());
    }

    public function getClient()
    {
        return $this->clientContext->getCurrent();
    }
}
