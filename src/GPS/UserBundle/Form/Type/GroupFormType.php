<?php

namespace GPS\UserBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Form\Type\GroupFormType as BaseType;
use GPS\TrackBundle\Doctrine\VehiculoManager;
use GPS\UserBundle\Doctrine\GroupManager;
use GPS\UserBundle\Doctrine\UserManager;
use GPS\UserBundle\Entity\Usuario;
use Symfony\Component\Form\FormBuilderInterface;

class GroupFormType extends BaseType
{
    /** @var VehiculoManager */
    private $vehiculoManager;

    /** @var UserManager */
    private $userManager;

    /** @var GroupManager */
    private $groupManager;

    public function __construct($class, UserManager $userManager, VehiculoManager $vehiculoManager, GroupManager $groupManager)
    {
        parent::__construct($class);

        $this->vehiculoManager = $vehiculoManager;
        $this->userManager = $userManager;
        $this->groupManager = $groupManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $vehiculoManager = $this->vehiculoManager;
        $userManager = $this->userManager;
        $groupManager = $this->groupManager;

        $builder->add('descripcion', null, array(
            'label' => 'form.description', 'translation_domain' => 'GPSUserBundle',
        ));

        $builder->add('roles', 'choice', array(
            'choices' => Usuario::$ROLES,
            'multiple' => true,
            'attr' => ['class' => 'chosen'],
            'label' => 'form.roles', 'translation_domain' => 'GPSUserBundle',
            'required' => false,
        ));

        $builder->add('parent', 'entity', array(
            'class' => 'GPSUserBundle:Grupo',
            'query_builder' => function (EntityRepository $er) use ($groupManager) {
                    return $groupManager->getQueryBuilder();
                },
            'attr' => ['class' => 'chosen'],
            'required' => false,
            'label' => 'form.parent', 'translation_domain' => 'GPSUserBundle',
        ));

        $builder->add('usuarios', 'entity', array(
            'class' => 'GPSUserBundle:Usuario',
            'query_builder' => function (EntityRepository $er) use ($userManager) {
                    return $userManager->getQueryBuilder();
                },
            'multiple' => true,
            'attr' => ['class' => 'chosen'],
            'required' => false,
            'label' => 'form.users', 'translation_domain' => 'GPSUserBundle',
        ));

        $builder->add('vehiculos', 'entity', array(
            'class' => 'GPSTrackBundle:Vehiculo',
            'query_builder' => function (EntityRepository $er) use ($vehiculoManager) {
                    return $vehiculoManager->getQueryBuilder();
                },
            'property' => 'trackerPlaca',
            'multiple' => true,
            'attr' => ['class' => 'chosen'],
            'required' => false,
            'label' => 'form.vehicles', 'translation_domain' => 'GPSUserBundle',
        ));
    }

    public function getName()
    {
        return 'gps_user_group';
    }
}
