<?php

namespace GPS\UserBundle\Form\Type;

use GPS\UserBundle\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle'))
            ->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
                'required' => false,
            ))
            ->add('roles', 'choice', array(
                'choices' => Usuario::$ROLES,
                'multiple' => true,
                'attr' => ['class' => 'chosen'],
                'label' => 'form.roles', 'translation_domain' => 'GPSUserBundle',
                'required' => false,
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPS\UserBundle\Entity\Usuario',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gps_user_usuario';
    }
}
