<?php

namespace GPS\UserBundle\Form\Type;

use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileFormType extends BaseType
{
    private $optionsForm;

    public function __construct($class, $optionsForm)
    {
        parent::__construct($class);

        $this->optionsForm = $optionsForm;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('opciones', $this->optionsForm);

        parent::buildForm($builder, $options);

        $builder->remove('username');
    }

    public function getName()
    {
        return 'gps_user_profile';
    }
}
