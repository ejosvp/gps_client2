<?php

namespace GPS\UserBundle\Form\Type;

use GPS\UserBundle\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OptionsFormType extends AbstractType
{
    private $homepages;

    public function __construct(array $homepages)
    {
        $this->homepages = $homepages;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('homepage', 'choice', array(
                'label' => 'form.homepage', 'translation_domain' => 'GPSUserBundle',
                'choices' => $this->homepages,
            ))
            ->add('mapType', 'choice', array(
                'label' => 'form.mapType', 'translation_domain' => 'GPSUserBundle',
                'choices' => Usuario::$MAP_TYPES,
            ))
        ;
    }

    public function getName()
    {
        return 'gps_user_options';
    }
}