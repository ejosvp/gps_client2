<?php

namespace GPS\UserBundle\Controller;

use FOS\UserBundle\Controller\GroupController as BaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class GroupController extends BaseController
{
    public function listAction()
    {
        $groups = $this->container->get('fos_user.group_manager')->findParents();

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Group:list.html.'.$this->getEngine(), array('groups' => $groups));
    }

    public function editAction($groupname)
    {
        $group = $this->findGroupBy('name', $groupname);
        $form = $this->container->get('fos_user.group.form');
        $formHandler = $this->container->get('fos_user.group.form.handler');

        $process = $formHandler->process($group);
        if ($process)
        {
            $this->setFlash('fos_user_success', 'group.flash.updated');
            $groupUrl = $this->container->get('router')->generate('fos_user_group_list');

            return new RedirectResponse($groupUrl);
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Group:edit.html.' . $this->getEngine(), array(
            'form' => $form->createview(),
            'groupname' => $group->getName(),
        ));
    }

    public function newAction()
    {
        $form = $this->container->get('fos_user.group.form');
        $formHandler = $this->container->get('fos_user.group.form.handler');

        $process = $formHandler->process();
        if ($process)
        {
            $this->setFlash('fos_user_success', 'group.flash.created');
            $url = $this->container->get('router')->generate('fos_user_group_list');

            return new RedirectResponse($url);
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Group:new.html.' . $this->getEngine(), array(
            'form' => $form->createview(),
        ));
    }

    /**
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set(
            $action,
            $this->container->get('translator')->trans($value, [], 'FOSUserBundle')
        );
    }
}
