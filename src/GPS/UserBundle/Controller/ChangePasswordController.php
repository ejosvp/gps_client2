<?php

namespace GPS\UserBundle\Controller;

use FOS\UserBundle\Controller\ChangePasswordController as BaseController;
use FOS\UserBundle\Model\UserInterface;

class ChangePasswordController extends BaseController
{
    protected function getRedirectionUrl(UserInterface $user)
    {
        return $this->container->get('router')->generate('fos_user_change_password');
    }

    /**
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set(
            $action,
            $this->container->get('translator')->trans($value, [], 'FOSUserBundle')
        );
    }
}
