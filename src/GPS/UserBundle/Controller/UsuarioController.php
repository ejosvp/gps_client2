<?php

namespace GPS\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\UserBundle\Entity\Usuario;
use GPS\UserBundle\Form\Type\UsuarioType;

/**
 * Usuario controller.
 *
 * @Route("/usuario")
 */
class UsuarioController extends Controller
{

    /**
     * Lists all Usuarios.
     *
     * @Route("/", name="usuario")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $usuarios = $this->get('gps_user.user_manager')->findUsers();

        return array(
            'usuarios' => $usuarios,
        );
    }

    /**
     * Creates a new Usuario.
     *
     * @Route("/", name="usuario_create")
     * @Method("POST")
     * @Template("GPSUserBundle:Usuario:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $usuario = $this->get('gps_user.user_manager')->createUser();
        $form = $this->createCreateForm($usuario);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $usuario->setEnabled(true);
            $this->get('gps_user.user_manager')->updateUser($usuario);

            $this->setFlash('success', 'user.flash.created');

            return $this->redirect($this->generateUrl('usuario'));
        }

        return array(
            'usuario' => $usuario,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Usuario.
     *
     * @param Usuario $usuario The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Usuario $usuario)
    {
        $form = $this->createForm(new UsuarioType(), $usuario, array(
            'action' => $this->generateUrl('usuario_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Crear',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Usuario.
     *
     * @Route("/new", name="usuario_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $usuario = $this->get('gps_user.user_manager')->createUser();
        $form = $this->createCreateForm($usuario);

        return array(
            'usuario' => $usuario,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Usuario.
     *
     * @Route("/{username}/edit", name="usuario_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($username)
    {
        $usuario = $this->get('gps_user.user_manager')->findUserByUsername($username);

        if (!$usuario)
        {
            throw $this->createNotFoundException('Unable to find Usuario.');
        }

        $form = $this->createEditForm($usuario);
        $deleteForm = $this->createDeleteForm($username);

        return array(
            'usuario' => $usuario,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Usuario.
     *
     * @param Usuario $usuario The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Usuario $usuario)
    {
        $form = $this->createForm(new UsuarioType(), $usuario, array(
            'action' => $this->generateUrl('usuario_update', array('username' => $usuario->getUsername())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }

    /**
     * Edits an existing Usuario.
     *
     * @Route("/{username}", name="usuario_update")
     * @Method("PUT")
     * @Template("GPSUserBundle:Usuario:edit.html.twig")
     */
    public function updateAction(Request $request, $username)
    {
        $usuario = $this->get('gps_user.user_manager')->findUserByUsername($username);

        if (!$usuario)
        {
            throw $this->createNotFoundException('Unable to find Usuario.');
        }

        $deleteForm = $this->createDeleteForm($username);
        $form = $this->createEditForm($usuario);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $this->get('gps_user.user_manager')->updateUser($usuario);

            $this->setFlash('success', 'user.flash.updated');

            return $this->redirect($this->generateUrl('usuario'));
        }

        return array(
            'usuario' => $usuario,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Usuario.
     *
     * @Route("/{username}", name="usuario_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $username)
    {
        $form = $this->createDeleteForm($username);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $usuario = $this->get('gps_user.user_manager')->findUserByUsername($username);

            if (!$usuario)
            {
                throw $this->createNotFoundException('Unable to find Usuario.');
            }

            if ($usuario == $this->get('security.context')->getToken()->getUser())
            {
                $this->setFlash('success', 'user.flash.delete_error');
                return $this->redirect($this->generateUrl('usuario'));
            }

            $this->get('gps_user.user_manager')->deleteUser($usuario);

            $this->setFlash('success', 'user.flash.deleted');
        }

        return $this->redirect($this->generateUrl('usuario'));
    }

    /**
     * Creates a form to delete a Usuario by username.
     *
     * @param mixed $username The usuario username
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($username)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuario_delete', array('username' => $username)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Eliminar',
                'attr' => array('class' => 'btn-danger')
            ))
            ->getForm();
    }

    /**
     * Enable/Disable a Usuario.
     *
     * @Route("/{username}/enable", name="usuario_enable")
     * @Method("GET")
     */
    public function enableAction($username)
    {
        $usuario = $this->get('gps_user.user_manager')->findUserByUsername($username);

        if (!$usuario)
        {
            throw $this->createNotFoundException('Unable to find Usuario.');
        }

        if ($usuario == $this->get('security.context')->getToken()->getUser())
        {
            $this->setFlash('success', 'user.flash.disable_error');
            return $this->redirect($this->generateUrl('usuario'));
        }

        $usuario->setEnabled(!$usuario->isEnabled());
        $this->get('gps_user.user_manager')->updateUser($usuario);

        $this->setFlash('success', 'user.flash.' . ($usuario->isEnabled() ? 'enabled' : 'disabled'));

        return $this->redirect($this->generateUrl('usuario'));
    }

    /**
     * @param $action
     * @param $value
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->set(
            $action,
            $this->container->get('translator')->trans($value, [], 'GPSUserBundle')
        );
    }
}
