<?php

namespace GPS\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\Group as BaseGroup;
use FOS\UserBundle\Model\GroupInterface;
use GPS\TrackBundle\Entity\Alarm;
use GPS\TrackBundle\Entity\Cliente;
use GPS\TrackBundle\Entity\Vehiculo;

/**
 * Grupo
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="name",
 *          column=@ORM\Column(
 *              name="name",
 *              type="string",
 *              length=255,
 *              unique=false
 *          )
 *      )
 * })
 */
class Grupo extends BaseGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Usuario", inversedBy="groups")
     */
    private $usuarios;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="GPS\TrackBundle\Entity\Vehiculo", inversedBy="groups")
     */
    private $vehiculos;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="GPS\TrackBundle\Entity\Alarm", inversedBy="alarms")
     */
    private $alarms;

    /**
     * @var Cliente
     *
     * @ORM\ManyToOne(targetEntity="GPS\TrackBundle\Entity\Cliente")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id", nullable=false)
     */
    private $cliente;

    /**
     * @ORM\OneToMany(targetEntity="GPS\UserBundle\Entity\Grupo", mappedBy="parent")
     **/
    private $groups;

    /**
     * @ORM\ManyToOne(targetEntity="GPS\UserBundle\Entity\Grupo", inversedBy="groups")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     **/
    private $parent;


    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->groups = new ArrayCollection();
    }

    public function __toString()
    {
        $depth = "";
        $parent = $this->getParent();
        while ($parent) {
            $depth .= "- ";
            $parent = $parent->getParent();
        }
        return (string) $this->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Grupo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set usuarios
     *
     * @param ArrayCollection $usuarios
     * @return Grupo
     */
    public function setUsuarios(ArrayCollection $usuarios)
    {
        $this->usuarios = $usuarios;

        return $this;
    }

    /**
     * Get usuarios
     *
     * @return ArrayCollection
     */
    public function getUsuarios()
    {
        return $this->usuarios ?: $this->usuarios = new ArrayCollection();
    }

    public function getUsuariosUsernames()
    {
        $usernames = array();
        foreach ($this->getUsuarios() as $usuario) {
            $usernames[] = $usuario->getUsername();
        }

        return $usernames;
    }

    public function hasUsuario($username)
    {
        return in_array($username, $this->getUsuariosUsernames());
    }

    public function addUsuario(Usuario $usuario)
    {
        if (!$this->getUsuarios()->contains($usuario)) {
            $this->getUsuarios()->add($usuario);
        }

        return $this;
    }

    public function removeUsuario(Usuario $usuario)
    {
        if ($this->getUsuarios()->contains($usuario)) {
            $this->getUsuarios()->removeElement($usuario);
        }

        return $this;
    }

    /**
     * Set vehiculos
     *
     * @param ArrayCollection $vehiculos
     * @return Grupo
     */
    public function setVehiculos(ArrayCollection $vehiculos)
    {
        $this->vehiculos = $vehiculos;

        return $this;
    }

    /**
     * Get vehiculos
     *
     * @return ArrayCollection
     */
    public function getVehiculos()
    {
        return $this->vehiculos ?: $this->vehiculos = new ArrayCollection();
    }

    public function getVehiculosPlacas()
    {
        $placas = array();
        foreach ($this->getVehiculos() as $vehiculo) {
            $placas[] = $vehiculo->getPlaca();
        }

        return $placas;
    }

    public function hasVehiculo($placa)
    {
        return in_array($placa, $this->getVehiculosPlacas());
    }

    public function addVehiculo(Vehiculo $vehiculo)
    {
        if (!$this->getVehiculos()->contains($vehiculo)) {
            $this->getVehiculos()->add($vehiculo);
        }

        return $this;
    }

    public function removeVehiculo(Vehiculo $vehiculo)
    {
        if ($this->getVehiculos()->contains($vehiculo)) {
            $this->getVehiculos()->removeElement($vehiculo);
        }

        return $this;
    }

    /**
     * Set cliente
     *
     * @param Cliente $cliente
     * @return Grupo
     */
    public function setCliente(Cliente $cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return Grupo
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    public function getGroupNames()
    {
        $names = array();
        foreach ($this->getGroups() as $group) {
            $names[] = $group->getName();
        }

        return $names;
    }

    public function hasGroup($name)
    {
        return in_array($name, $this->getGroupNames());
    }

    public function addGroup(GroupInterface $group)
    {
        if (!$this->getGroups()->contains($group)) {
            $this->getGroups()->add($group);
        }

        return $this;
    }

    public function removeGroup(GroupInterface $group)
    {
        if ($this->getGroups()->contains($group)) {
            $this->getGroups()->removeElement($group);
        }

        return $this;
    }

    public function getRoles()
    {
        $roles = $this->roles;

        if ($this->getParent() !== null)
            $roles = array_merge($roles, $this->getParent()->getRoles());

        return array_unique($roles);
    }

    /**
     * @return mixed
     */
    public function getAlarms()
    {
        return $this->alarms;
    }

    public function getAlarmNames()
    {
        $names = array();
        foreach ($this->getAlarms() as $alarm) {
            $names[] = $alarm->getName();
        }

        return $names;
    }

    public function hasAlarm($name)
    {
        return in_array($name, $this->getAlarmNames());
    }

    public function addAlarm(Alarm $alarm)
    {
        if (!$this->getAlarms()->contains($alarm)) {
            $this->getAlarms()->add($alarm);
        }

        return $this;
    }

    public function removeAlarm(Alarm $alarm)
    {
        if ($this->getAlarms()->contains($alarm)) {
            $this->getAlarms()->removeElement($alarm);
        }

        return $this;
    }
}
