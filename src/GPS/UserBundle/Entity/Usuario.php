<?php

namespace GPS\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;
use GPS\TrackBundle\Entity\Cliente;
use GPS\UserBundle\Entity\Grupo;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="usernameCanonical",
 *          column=@ORM\Column(
 *              name="usernameCanonical",
 *              type="string",
 *              length=255,
 *              unique=false
 *          )
 *      )
 * })
 */
class Usuario extends BaseUser
{
    const ROLE_DEFAULT = 'ROLE_DEFAULT';

    const MAP_HYBRYD = 'HYBRYD';
    const MAP_ROADMAP = 'ROADMAP';
    const MAP_SATELLITE = 'SATELLITE';
    const MAP_TERRAIN = 'TERRAIN';

    static $MAP_TYPES = array(
        self::MAP_ROADMAP => 'Calles',
        self::MAP_HYBRYD => 'Híbrido',
        self::MAP_SATELLITE => 'Satélite',
        self::MAP_TERRAIN => 'Terreno',
    );

    static $ROLES = array(
        'ROLE_ADMIN' => 'role.admin',
        'ROLE_BASIC' => 'role.basic',
        'ROLE_DESKTOP' => 'role.desktop',
        'role.group' => array(
            'ROLE_GROUP' => 'role.group',
            'ROLE_GROUP_LIST' => 'role.group.list',
            'ROLE_GROUP_NEW' => 'role.group.new',
            'ROLE_GROUP_EDIT' => 'role.group.edit',
            'ROLE_GROUP_DELETE' => 'role.group.delete',
        ),
        'role.user' => array(
            'ROLE_USER' => 'role.user',
            'ROLE_USER_LIST' => 'role.user.list',
            'ROLE_USER_NEW' => 'role.user.new',
            'ROLE_USER_EDIT' => 'role.user.edit',
            'ROLE_USER_DELETE' => 'role.user.delete',
            'ROLE_USER_ENABLE' => 'role.user.enable',
        ),
        'role.tracker' => array(
            'ROLE_TRACKER' => 'role.tracker',
            'ROLE_TRACKER_LIST' => 'role.tracker.list',
            'ROLE_TRACKER_NEW' => 'role.tracker.new',
            'ROLE_TRACKER_EDIT' => 'role.tracker.edit',
            'ROLE_TRACKER_DELETE' => 'role.tracker.delete',
        ),
        'role.vehicle' => array(
            'ROLE_VEHICLE' => 'role.vehicle',
            'ROLE_VEHICLE_LIST' => 'role.vehicle.list',
            'ROLE_VEHICLE_NEW' => 'role.vehicle.new',
            'ROLE_VEHICLE_EDIT' => 'role.vehicle.edit',
            'ROLE_VEHICLE_DELETE' => 'role.vehicle.delete',
        ),
        'role.geozone' => array(
            'ROLE_GEOZONE' => 'role.geozone',
            'ROLE_GEOZONE_LIST' => 'role.geozone.list',
            'ROLE_GEOZONE_NEW' => 'role.geozone.new',
            'ROLE_GEOZONE_EDIT' => 'role.geozone.edit',
            'ROLE_GEOZONE_DELETE' => 'role.geozone.delete',
        ),
        'role.driver' => array(
            'ROLE_DRIVER' => 'role.driver',
            'ROLE_DRIVER_LIST' => 'role.driver.list',
            'ROLE_DRIVER_NEW' => 'role.driver.new',
            'ROLE_DRIVER_EDIT' => 'role.driver.edit',
            'ROLE_DRIVER_DELETE' => 'role.driver.delete',
        ),
    );

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**

    /**
     * @var Grupo[]
     *
     * @ORM\ManyToMany(targetEntity="Grupo", mappedBy="usuarios")
     */
    protected $groups;

    /**
     * @var array
     *
     * @ORM\Column(name="opciones", type="array")
     */
    private $opciones;

    /**
     * @var Cliente
     *
     * @ORM\ManyToOne(targetEntity="GPS\TrackBundle\Entity\Cliente")
     */
    private $cliente;


    public function __construct()
    {
        parent::__construct();

        $this->opciones = array(
            'homepage' => 'escritorio',
            'mapType' => self::MAP_ROADMAP
        );
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set opciones
     *
     * @param array $opciones
     * @return Usuario
     */
    public function setOpciones($opciones)
    {
        $this->opciones = $opciones;

        return $this;
    }

    /**
     * Get opciones
     *
     * @return array
     */
    public function getOpciones()
    {
        return $this->opciones;
    }

    /**
     * Set cliente
     *
     * @param Cliente $cliente
     * @return Usuario
     */
    public function setCliente(Cliente $cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    public function getRoleNames()
    {
        $names = array();
        foreach ($this->getRoles() as $role) {
            $names[] = strtolower(substr($role, 5));
        }

        return $names;
    }
}
