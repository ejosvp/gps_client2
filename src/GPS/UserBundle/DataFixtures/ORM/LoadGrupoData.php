<?php

namespace GPS\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\GroupManager;
use GPS\UserBundle\Entity\Grupo;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadGrupoData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /** @var $manager GroupManager */
        $manager = $this->container->get('fos_user.group_manager');

        $groups = array(
            array('ruta 1', 0),
            array('ruta dós', 0),
            array('ejecutivos', 0),
            array('comite sur', 1),
            array('comite norte', 1),
            array('ejecutivos', 1),
        );

        foreach ($groups as $group)
        {
            /** @var Grupo $entity */
            $entity = $manager->createGroup($group[0]);
            $entity->setCliente($this->getReference('cliente-' . $group[1]));
            $entity->addUsuario($this->getReference('user-' . $group[1] . '-admin'));
            for($j = 0; $j < 5; $j++)
            {
                $entity->addVehiculo($this->getReference('vehiculo-' . $j*2));
            }
            $manager->updateGroup($entity);

            $this->addReference('grupo-' . $group[1] . '-' . $group[0], $entity);
        }
    }

    function getOrder()
    {
        return 100;
    }
}
