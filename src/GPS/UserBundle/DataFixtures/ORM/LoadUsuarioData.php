<?php

namespace GPS\TrackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use FOS\UserBundle\Util\UserManipulator;
use GPS\TrackBundle\Entity\Usuario;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /** @var $manipulator UserManipulator */
        $manipulator = $this->container->get('fos_user.util.user_manipulator');

        $users = [
            ['admin', 'ROLE_ADMIN', 0],
            ['combis', 'ROLE_BASIC', 0],
            ['admin', 'ROLE_ADMIN', 1],
            ['taxis', 'ROLE_BASIC', 1],
        ];

        foreach ($users as $user)
        {
            /** @var Usuario $entity */
            $entity = $manipulator->create($user[0], $user[0], $user[0] . $user[2] . '@gpstrack.com', true, false);
            $entity->setCliente($this->getReference('cliente-'. $user[2]));
            $manipulator->addRole($user[0], $user[1]);

            $this->addReference('user-' . $user[2] . '-' . $user[0], $entity);
        }

        $admin = $manipulator->create('admin', 'admin', 'super_admin@gpstrack.com', true, true);
        $admin->setCliente($this->getReference('cliente-admin'));
        $this->container->get('fos_user.user_manager')->updateUser($admin, true);
    }

    function getOrder()
    {
        return 5;
    }
}
