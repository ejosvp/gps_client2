<?php

namespace GPS\TrackBundle\Util;


abstract class PlotDecoder
{
    static function hexToBin($hex)
    {
        if ($hex == "0000") return array();
        $p = sprintf("%%0%sb", strlen($hex));
        return str_split(sprintf($p, hexdec($hex)));
    }
} 