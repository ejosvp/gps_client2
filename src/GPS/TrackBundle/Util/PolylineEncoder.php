<?php

/**
 * PolylineEncoder by Jim Hribar
 *
 * http://facstaff.unca.edu/mcmcclur/GoogleMaps/EncodePolyline/PolylineEncoder.php.txt
 */

namespace GPS\TrackBundle\Util;


class PolylineEncoder
{

    private $numLevels;
    private $zoomFactor;
    private $verySmall;
    private $forceEndpoints;
    private $zoomLevelBreaks;

    public function __construct()
    {
        $this->numLevels = 18;
        $this->zoomFactor = 2;
        $this->verySmall = 0.00001;
        $this->forceEndpoints = true;


        for ($i = 0; $i < $this->numLevels; $i++) {
            $this->zoomLevelBreaks[$i] = $this->verySmall * pow($this->zoomFactor, $this->numLevels - $i - 1);
        }
    }

    function computeLevel($dd)
    {
        $lev = 0;

        if ($dd > $this->verySmall) {
            $lev = 0;
            while ($dd < $$this->zoomLevelBreaks[$lev]) {
                $lev++;
            }
        }
        return $lev;
    }

    function encodeSignedNumber($num)
    {
        $sgn_num = $num << 1;
        if ($num < 0) {
            $sgn_num = ~($sgn_num);
        }
        return $this->encodeNumber($sgn_num);
    }

    function createEncodings($points, $dists)
    {
        $encoded_points = "";

        $plat = 0;
        $plng = 0;

        for ($i = 0; $i < count($points); $i++) {
            if (isset($dists[$i]) || $i == 0 || $i == count($points) - 1) {
                $point = $points[$i];
                $lat = $point[0];
                $lng = $point[1];
                $late5 = floor($lat * 1e5);
                $lnge5 = floor($lng * 1e5);
                $dlat = $late5 - $plat;
                $dlng = $lnge5 - $plng;
                $plat = $late5;
                $plng = $lnge5;
                $encoded_points .= $this->encodeSignedNumber($dlat) . $this->encodeSignedNumber($dlng);
            }
        }
        return $encoded_points;
    }

    function encodeLevels($points, $dists, $absMaxDist)
    {
        $encoded_levels = "";

        if ($this->forceEndpoints) {
            $encoded_levels .= $this->encodeNumber($this->numLevels - 1);
        } else {
            $encoded_levels .= $this->encodeNumber($this->numLevels - $this->computeLevel($absMaxDist) - 1);
        }
        for ($i = 1; $i < count($points) - 1; $i++) {
            if (isset($dists[$i])) {
                $encoded_levels .= $this->encodeNumber($this->numLevels - $this->computeLevel($dists[$i]) - 1);
            }
        }
        if ($this->forceEndpoints) {
            $encoded_levels .= $this->encodeNumber($this->numLevels - 1);
        } else {
            $encoded_levels .= $this->encodeNumber($this->numLevels - $this->computeLevel($absMaxDist) - 1);
        }
        return $encoded_levels;
    }

    function encodeNumber($num)
    {
        $encodeString = "";
        
        while ($num >= 0x20) {
            $nextValue = (0x20 | ($num & 0x1f)) + 63;
            $encodeString .= chr($nextValue);
            $num >>= 5;
        }
        $finalValue = $num + 63;
        $encodeString .= chr($finalValue);
        return $encodeString;
    }

}