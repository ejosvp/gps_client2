<?php

namespace GPS\TrackBundle\Doctrine;

use Doctrine\ORM\EntityManager;
use GPS\TrackBundle\Security\ClientContext;
use Symfony\Component\Security\Core\SecurityContextInterface;

class VehiculoManager extends AbstractManager
{
    private $securityContext;

    private $user;

    public function __construct(ClientContext $clientContext, SecurityContextInterface $securityContext, EntityManager $em, $class)
    {
        parent::__construct($clientContext, $em, $class);

        $this->securityContext = $securityContext;
        $this->user = $securityContext->getToken()->getUser();
    }

    public function findAllWithLastPlot(\DateTime $time = null, $currentUser = true)
    {
        $q = $this->entityManager->createQueryBuilder()
            ->select('
            v.id, v.placa,
            t.codigo,
            p.id AS p_id, p.timestamp, p.latitude, p.longitude, p.speed, p.course, p.extendedInfo')
            ->from('GPS\\TrackBundle\\Entity\\Trama', 'p')
            ->leftJoin('p.tracker', 't')
            ->leftJoin('t.vehiculo', 'v')
            ->leftJoin('v.groups', 'g')
            ->groupBy('p.tracker')
            ->where('p.id IN (SELECT MAX(pmax.id) FROM GPS\\TrackBundle\\Entity\\Trama pmax GROUP BY pmax.tracker)')
            ->andWhere('v.cliente = :client')
            ->setParameter('client', $this->getCliente());

        if ($currentUser) {
            $q->andWhere(':user MEMBER OF g.usuarios');
            $q->setParameter('user', $this->getUser());
        }

        if ($time !== null) {
            $q->andWhere('p.timestamp > :time');
            $q->setParameter('time', $time->format('Y-m-d H:i:s'));
        }

        return $q->getQuery()->getArrayResult();
    }

    public function getGroupNames($currentUser = true)
    {
        $q = $this->getQueryBuilder()
            ->select('v.placa, g.name as group_name')
            ->leftJoin('v.groups', 'g');

        if ($currentUser) {
            $q->andWhere(':user MEMBER OF g.usuarios');
            $q->setParameter('user', $this->getUser());
        }

        $vehicleGroups = $q->getQuery()->getArrayResult();

        foreach ($vehicleGroups as $key => $vehicle) {
            $vehicleGroups[$vehicle['placa']][] = $vehicle['group_name'];
            unset($vehicleGroups[$key]);
        }

        return $vehicleGroups;
    }

    public function getQueryBuilder()
    {
        return $this->repository->createQueryBuilder('v')
            ->where('v.cliente = :cliente')
            ->setParameter('cliente', $this->getCliente());
    }

    private function getUser()
    {
        return $this->user;
    }
}