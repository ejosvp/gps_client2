<?php

namespace GPS\TrackBundle\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use GPS\TrackBundle\Entity\Cliente;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ClienteManager implements ManagerInterface
{
    /** @var EntityManager */
    protected $entityManager;

    /** @var string */
    protected $class;

    /** @var EntityRepository */
    protected $repository;

    public function __construct(EntityManager $em, $class)
    {
        $this->entityManager = $em;
        $this->repository = $em->getRepository($class);

        $metadata = $em->getClassMetadata($class);
        $this->class = $metadata->getName();
    }

    public function create()
    {
        $class = $this->getClass();
        $entity = new $class;

        return $entity;
    }

    public function delete($entity)
    {
        if ($entity->getNombre() == 'admin') { return null; }

        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    /**
     * @param $id
     * @return Cliente
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findAll()
    {
        return $this->findBy(array());
    }

    public function getClass()
    {
        return $this->class;
    }

    public function reload($entity)
    {
        $this->entityManager->refresh($entity);
    }

    /**
     * @param Cliente $entity
     * @param bool $andFlush
     * @return null
     */
    public function update($entity, $andFlush = true)
    {
        if ($entity->getNombre() == 'admin') { return null; }

        if (null !== $entity->getFile()) {
            $entity->upload();
        }

        $this->entityManager->persist($entity);
        if ($andFlush) {
            $this->entityManager->flush();
        }
    }
}