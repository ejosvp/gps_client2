<?php

namespace GPS\TrackBundle\Doctrine;

use Doctrine\ORM\EntityManager;
use GPS\TrackBundle\Security\ClientContext;
use Symfony\Component\Security\Core\SecurityContextInterface;

class TramaManager extends AbstractManager
{
    private $securityContext;

    private $user;

    public function __construct(ClientContext $clientContext, SecurityContextInterface $securityContext, EntityManager $em, $class)
    {
        parent::__construct($clientContext, $em, $class);

        $this->securityContext = $securityContext;
        $this->user = $securityContext->getToken()->getUser();
    }

    public function findAllPlots(\DateTime $time = null)
    {
        $q = $this->repository->createQueryBuilder('p')
            ->select('p.id, p.course, p.latitude, p.longitude, v.placa, p.timestamp, p.speed, p.extendedInfo')
            ->leftJoin('p.tracker', 't')
            ->leftJoin('t.vehiculo', 'v')
            ->leftJoin('v.groups', 'g')
            ->orderBy('p.timestamp', 'DESC')
            ->where('v.cliente = :client')
            ->andWhere(':user MEMBER OF g.usuarios')
            ->setParameters(array(
                'client' => $this->getCliente(),
                'user' => $this->getUser()
            ));
        if ($time !== null) {
            $q->andWhere('p.timestamp > :time');
            $q->setParameter('time', $time->format('Y-m-d H:i:s'));
        }

        return $q->getQuery()->execute();
    }

    private function getUser()
    {
        return $this->user;
    }
}