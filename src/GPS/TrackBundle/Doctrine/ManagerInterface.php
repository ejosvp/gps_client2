<?php

namespace GPS\TrackBundle\Doctrine;

interface ManagerInterface
{
    public function create();

    public function delete($entity);

    public function find($id);

    public function findOneBy(array $criteria);

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    public function findAll();

    public function getClass();

    public function reload($entity);

    public function update($entity);
}
