<?php

namespace GPS\TrackBundle\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use GPS\TrackBundle\Security\ClientContext;

abstract class AbstractManager implements ManagerInterface
{
    /** @var ClientContext */
    protected $clientContext;

    /** @var EntityManager */
    protected $entityManager;

    /** @var string */
    protected $class;

    /** @var EntityRepository */
    protected $repository;

    public function __construct(ClientContext $clientContext, EntityManager $em, $class)
    {
        $this->clientContext = $clientContext;

        $this->entityManager = $em;
        $this->repository = $em->getRepository($class);

        $metadata = $em->getClassMetadata($class);
        $this->class = $metadata->getName();
    }

    public function create()
    {
        $class = $this->getClass();
        $entity = new $class;
        $entity->setCliente($this->getCliente());

        return $entity;
    }

    public function delete($entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function findOneBy(array $criteria)
    {
        $criteria = array_merge($criteria, ['cliente' => $this->getCliente()]);
        return $this->repository->findOneBy($criteria);
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $criteria = array_merge($criteria, ['cliente' => $this->getCliente()]);
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findAll()
    {
        return $this->findBy(array());
    }

    public function getClass()
    {
        return $this->class;
    }

    public function reload($entity)
    {
        $this->entityManager->refresh($entity);
    }

    public function update($entity, $andFlush = true)
    {
        $this->entityManager->persist($entity);
        if ($andFlush)
        {
            $this->entityManager->flush();
        }
    }

    public function getCliente()
    {
        return $this->clientContext->getCurrent();
    }
}