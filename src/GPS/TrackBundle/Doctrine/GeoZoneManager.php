<?php

namespace GPS\TrackBundle\Doctrine;

use GPS\TrackBundle\Entity\GeoZone;

class GeoZoneManager extends AbstractManager
{
    public function updateBounds(GeoZone $geozone)
    {
        $serialized = $geozone->getSerialized();
        if (null === $serialized) {
            $serialized = new \ArrayObject();
        }
        $data = json_decode($serialized, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);
        $geozone->setData($data);
    }

    public function update($geozone, $andFlush = true)
    {
        $this->updateBounds($geozone);

        parent::update($geozone, $andFlush);
    }
}