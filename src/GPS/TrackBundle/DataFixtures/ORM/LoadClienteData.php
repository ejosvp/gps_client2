<?php

namespace GPS\TrackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use GPS\TrackBundle\Entity\Cliente;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadClienteData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('es_AR');

        $admin = new Cliente();
        $admin->setNombre('admin');
        $this->addReference('cliente-admin', $admin);

        $manager->persist($admin);

        $entity = new Cliente();
        $entity->setNombre('combis');
        $this->addReference('cliente-0', $entity);

        $manager->persist($entity);

        for ($i = 1; $i < 10; $i++)
        {
            $entity = new Cliente();
            $entity->setNombre($faker->userName);

            $this->addReference('cliente-' . $i, $entity);

            $manager->persist($entity);
        }
        $manager->flush();
    }

    function getOrder()
    {
        return 0;
    }
}