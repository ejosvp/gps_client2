<?php

namespace GPS\TrackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use GPS\TrackBundle\Entity\Tracker;

class LoadTrackerData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('es_AR');

        for ($i = 0; $i < 100; $i++)
        {
            $tracker = new Tracker();
            $tracker->setMarca('MEITRACK');
            $tracker->setModelo('VT3' . $faker->randomElement([1, 4]) . '0');
            $tracker->setImei($faker->randomNumber(14));
            $tracker->setChip($faker->phoneNumber);
            $tracker->setCodigo($i);
            $tracker->setCliente($this->getReference('cliente-' . 0));

            $this->setReference('tracker-' . $i, $tracker);

            $manager->persist($tracker);
        }
        $manager->flush();
    }

    function getOrder()
    {
        return 20;
    }
}