<?php

namespace GPS\TrackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use GPS\TrackBundle\Entity\Vehiculo;

class LoadVehiculoData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++)
        {
            $vehiculo = new Vehiculo();
            $vehiculo->setPlaca('AH-123' . $i);
            $vehiculo->setTracker($this->getReference('tracker-' . $i));
            $vehiculo->setIcono('1');
            $vehiculo->setSeguro('580302443' . $i);
            $vehiculo->setSeguroConpania('Pacífico Seguros');
            $vehiculo->setSeguroExpira(new \DateTime());
            $vehiculo->setCliente($this->getReference('cliente-' . $i % 2));

            $this->setReference('vehiculo-' . $i, $vehiculo);

            $manager->persist($vehiculo);
        }
        $manager->flush();
    }

    function getOrder()
    {
        return 25;
    }
}