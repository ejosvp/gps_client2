<?php

namespace GPS\TrackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use GPS\TrackBundle\Entity\Chofer;

class LoadChoferData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('es_AR');

        for ($i = 0; $i < 100; $i++)
        {
            $chofer = new Chofer();
            $chofer->setNombre($faker->firstName);
            $chofer->setApellido($faker->lastName);
            $chofer->setDni($dni = $faker->randomNumber(8));
            $chofer->setLicencia($faker->randomNumber(10));
            $chofer->setLicenciaTipo(
                $faker->randomElement(['A', 'B']) // Tipo
                . $faker->randomElement([' I', ' II']) // Clase
                . $faker->randomElement(['-a', '-b']) // Mod
            );
            $chofer->setLicenciaExpira($faker->dateTimeBetween($startDate = 'now', $endDate = '+10 months'));
            $chofer->setTipoSangre(
                $faker->randomElement(['O', 'A', 'B', 'AB']) // Grupo
                . $faker->randomElement(['+', '-']) // Factor
            );
            $chofer->setFoto($dni . ".png");
            $chofer->setCliente($this->getReference('cliente-' . $faker->randomNumber(0, 9)));

            $this->setReference('chofer-' . $i, $chofer);

            $manager->persist($chofer);
        }
        $manager->flush();
    }

    function getOrder()
    {
        return 15;
    }
}