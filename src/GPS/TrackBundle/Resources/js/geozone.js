var overlays = [];
var colorInput = $('#gps_trackbundle_geozone_color');
var typeInput = $('#gps_trackbundle_geozone_type');

$(function () {
    colorInput.attr('type', 'color');
    map.map.setZoom(5);
});

$('form').on('submit', function (e) {
    var type = $('#gps_trackbundle_geozone_type option:selected').val();

    if (type == 0) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    }

    var serializedData;

    if (type == 1) {
        serializedData = '{"bounds":' +
            overlays.rectangle.getBounds().toString().replace(/\(/g, "[").replace(/\)/g, "]") +
            "}";
    }
    else if (type == 2) {
        serializedData = '{"center":' +
            overlays.circle.getCenter().toString().replace(/\(/g, "[").replace(/\)/g, "]") +
            ', "radius":' + overlays.circle.getRadius() +
            "}";
    }
    else if (type == 3) {
        points = [];
        var path = overlays.polygon.getPath().getArray();
        for (var i in path) {
            points.push(path[i].toString());
        }
        serializedData = '{"paths":[' + points.join().replace(/\(/g, "[").replace(/\)/g, "]") + ']}';
    }

    $('#gps_trackbundle_geozone_serialized').val(serializedData);

    return true;
});

typeInput.on('change', function () {
    var type = $('#gps_trackbundle_geozone_type option:selected').val();
    if (type == 1) geozone_rectangle(map);
    else if (type == 2) geozone_circle(map);
    else if (type == 3) geozone_complex(map);
});

colorInput.on('change', function () {
    var color = $(this).val() ? $(this).val() : '#000000';
    for (var i in overlays) {
        overlays[i].setOptions({
            fillColor: color,
            strokeColor: color
        });
    }
});

function clear_map() {
    for (var i in overlays) {
        overlays[i].setMap(null);
    }
}

function geozone_rectangle(app, bounds) {
    clear_map();

    var color = colorInput.val() ? colorInput.val() : '#000000';
    overlays.rectangle = new google.maps.Rectangle({
        map: app.map,
        bounds: bounds ? bounds : app.map.getBounds(),
        draggable: true,
        editable: true,
        strokeWeight: 2,
        fillColor: color,
        strokeColor: color
    });

    app.map.fitBounds(overlays.rectangle.getBounds());
}

function geozone_circle(app, center, radius) {
    clear_map();

    if (!radius) {
        radius = google.maps.geometry.spherical.computeDistanceBetween(
            app.map.getBounds().getNorthEast(),
            app.map.getBounds().getSouthWest()
        ) / 3;
    }

    var color = colorInput.val() ? colorInput.val() : '#000000';
    overlays.circle = new google.maps.Circle({
        center: center ? center : app.map.getCenter(),
        map: app.map,
        radius: radius,
        draggable: true,
        editable: true,
        strokeWeight: 2,
        fillColor: color,
        strokeColor: color
    });

    app.map.fitBounds(overlays.circle.getBounds());
}

function geozone_complex(app, paths) {
    clear_map();

    var color = colorInput.val() ? colorInput.val() : '#000000';
    overlays.polygon = new google.maps.Polygon({
        map: app.map,
        paths: paths ? paths : explodeBounds(app.map.getBounds()),
        draggable: true,
        editable: true,
        strokeWeight: 2,
        fillColor: color,
        strokeColor: color,
        geodesic: true
    });

    var bounds = new google.maps.LatLngBounds();
    var path = overlays.polygon.getPath().getArray();
    for (i in path) {
        bounds.extend(path[i]);
    }
    app.map.fitBounds(bounds);
}

function toPoint(point) {
    return new google.maps.LatLng(point[0], point[1])
}

function toBounds(points) {
    return new google.maps.LatLngBounds(toPoint(points[0]), toPoint(points[1]));
}
function toPaths(points) {
    var paths = [];
    for (var i in points) {
        paths.push(new google.maps.LatLng(points[i][0], points[i][1]));
    }
    return paths;
}

function explodeBounds(bounds) {
    return [
        bounds.getSouthWest(),
        new google.maps.LatLng(bounds.getSouthWest().lat(), bounds.getNorthEast().lng()),
        bounds.getNorthEast(),
        new google.maps.LatLng(bounds.getNorthEast().lat(), bounds.getSouthWest().lng())
    ];
}