// Layer
var Layer = function (map) {
    this.map = map;
    this.elements = [];
};

Layer.prototype.push = function (element) {
    this.elements.push(element);
};

Layer.prototype.remove = function (element) {
    var i = this.elements.indexOf(element);
    if (i >= 0)
        this.elements.slice(i, 1);
};

Layer.prototype.show = function () {
    if (this.elements) {
        for (i in this.elements) {
            this.elements[i].setMap(this.map);
        }
    }
};

Layer.prototype.hide = function () {
    if (this.elements) {
        for (i in this.elements) {
            this.elements[i].setMap(null);
        }
    }
};

Layer.prototype.clear = function () {
    this.hide();
    this.elements.length = 0;
};