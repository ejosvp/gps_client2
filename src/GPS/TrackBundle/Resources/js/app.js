/**
 * Tracker
 *
 * @param id
 * @param map
 * @param pathColor
 * @constructor
 */
var Tracker = function (id, map, pathColor) {
    this.map = map;
    this.id = id;
    this.points = {};
    this.head = null;
    this.path = null;
    this.pathColor = pathColor;
};

Tracker.prototype.addPoint = function (id, plot) {
    if (this.points[id] != null)
        this.points[id].setMap(null);

    var infoWindow = new google.maps.InfoWindow({
        content: plot.info,
        'disableAutoPan': true
    });

    var marker = this.head = new google.maps.Marker({
        position: new google.maps.LatLng(plot.latitude, plot.longitude),
        map: this.map,
        icon: plot.icon,
        plot: id,
        infoWindow: infoWindow
    });

    google.maps.event.addListener(marker, 'mouseover', function () {
        infoWindow.open(marker.getMap(), marker);
    });

    google.maps.event.addListener(marker, 'mouseout', function () {
        infoWindow.close();
    });

    this.points[id] = marker;

    return marker;
};

Tracker.prototype.removePoint = function (plot) {
    if (this.points[plot] != null && this.head.plot != plot) {
        this.points[plot].setMap(null);
        delete this.points[plot];
    }
};

Tracker.prototype.centerHead = function () {
    this.map.setCenter(this.head.getPosition());
};

Tracker.prototype.show = function (force) {
    for (var i in this.points) {
        this.points[i].setMap(this.map);
    }
    this.showPath(force);
};

Tracker.prototype.hide = function () {
    for (var i in this.points) {
        this.points[i].setMap(null);
    }
    this.hidePath();
};

Tracker.prototype.showPath = function (force) {
    var makePath = function (tracker) {
        if (tracker.path != null) {
            tracker.hidePath();
            delete tracker.path;
        }

        var pathPoints = [];
        for (var i in tracker.points) {
            if (tracker.points[i].getMap()) // is_visible ?
                pathPoints.push(tracker.points[i].getPosition());
        }

        tracker.path = new google.maps.Polyline({
            path: pathPoints,
            map: tracker.map,
            geodesic: true,
            strokeColor: tracker.pathColor,
            strokeOpacity: 0.7,
            strokeWeight: 3
        });
    };

    if (this.path == null || force)
        makePath(this);
    else
        this.path.setMap(this.map);
};

Tracker.prototype.hidePath = function () {
    this.path.setMap(null);
};

Tracker.prototype.fit = function () {
    var latlngbounds = new google.maps.LatLngBounds();
    for (var i in this.points) {
        latlngbounds.extend(this.points[i].getPosition());
    }
    this.map.fitBounds(latlngbounds);
};

/**
 * Application
 *
 * @param element
 * @param type
 * @constructor
 */
var Map = function (element, type) {
    this.map = null;
    this.selector = document.getElementById(element);
    this.mapType = type;
    this.trackers = {};
};

Map.prototype.run = function () {
    this.map = new google.maps.Map(this.selector, {
        'zoom': 2,
        'center': new google.maps.LatLng(0, 0),
        'mapTypeId': google.maps.MapTypeId[this.mapType]
    });
};

Map.prototype.addTracker = function (id) {
    if (!this.trackers[id])
        return this.trackers[id] = new Tracker(id, this.map, 'FF0000');
};

Map.prototype.getTracker = function (id) {
    return this.trackers[id];
};

Map.prototype.fit = function () {
    var latlngbounds = new google.maps.LatLngBounds();
    for (var i in this.trackers) {
        var points = this.trackers[i].points;
        for (var j in points) {
            if (points[j].getMap()) // is_visible ?
                latlngbounds.extend(points[j].getPosition());
        }
    }
    this.map.fitBounds(latlngbounds);
};

function getURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
    );
}