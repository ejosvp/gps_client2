var lastUpdate = new Date();
var timer, pause;
var url;
var delay;
var follow = false;

var iconDirection = function (course, velocity) {
    var color = "a";
    var text = "·";

    if (velocity == 0) color = "b";
    else {
        course = (course + 360) % 360;

        if (course <= 22.5 || course > 337.5) text = "\u2192";
        else if (course <= 67.5)             text = "\u2197";
        else if (course <= 112.5)            text = "\u2191";
        else if (course <= 157.5)            text = "\u2196";
        else if (course <= 202.5)            text = "\u2190";
        else if (course <= 247.5)            text = "\u2199";
        else if (course <= 292.5)            text = "\u2193";
        else if (course <= 337.5)            text = "\u2198";
    }

    return "http://mt.google.com/vt/icon/text=" + text + "&psize=24&font=fonts/arialuni_t.ttf&color=ff000000&name=icons/spotlight/spotlight-waypoint-" + color + ".png&ax=44&ay=48&scale=1";
};

var loadTrackers = function (app, trackers) {
    for (var i in trackers) {
        app.addTracker(i);
        var tracker = app.trackers[i];
        for (var j in trackers[i]) {
            if (!tracker.points[j]) {
                var plot = trackers[i][j];
                plot.icon = iconDirection(plot.course);
                tracker.addPoint(j, plot);
            }
        }
        tracker.showPath(true);
    }
};

var startLoop = function () {
    timer = setInterval(function () {
        $.ajax({
            data: { date: stringDate(lastUpdate) },
            dataType: "json",
            error: function (xhr, status, error) {
                console.log("error");
                console.log(status);
                console.log(error);
            },
            success: function (data, status, xhr) {
                loadTrackers(map, data.allPlots);
                lastUpdate = new Date();
                if (follow) {
                    map.getTracker(follow).centerHead();
                }
            },
            type: "POST",
            url: url
        });
    }, delay);
};

// 'Y-m-d H:i:s'
var stringDate = function (date) {
    var r = '';

    r += date.getFullYear();
    r += "-" + ('0' + (date.getMonth() + 1)).slice(-2);
    r += "-" + ('0' + date.getDate()).slice(-2);
    r += " " + ('0' + date.getHours()).slice(-2);
    r += ":" + ('0' + date.getMinutes()).slice(-2);
    r += ":" + ('0' + date.getSeconds()).slice(-2);

    return r;
};

$(function () {
    $(window).resize(function () {
        $("#map_canvas").height($(window).height() - 50)
    });

    var groups = $('#desktop_filter_groups');

    $('.vehicle-show').on('click', function () {
        var vehicle = $($(this).parents("[data-vehicle]")[0]).attr('data-vehicle');
        map.getTracker(vehicle).fit();
    });

    $('.vehicle-follow').on('click', function () {
        var vehicle = $($(this).parents("[data-vehicle]")[0]).attr('data-vehicle');
        if (follow == vehicle) {
            $(this).removeClass('glyphicon-stop')
            $(this).addClass('glyphicon-play')
            follow = false;
        } else {
            $(this).removeClass('glyphicon-play')
            $(this).addClass('glyphicon-stop')
            follow = vehicle;
            map.getTracker(vehicle).centerHead();
        }
    });

    $('#desktop_filter_reset').on('click', function () {
        groups.children('option:selected').each(function () {
            $(this).removeProp('selected');
        });
        groups.trigger("chosen:updated").trigger("change");
        map.fit();
    });

    $('#desktop_filter_center').on('click', function () {
        map.fit();
    });

    $('#desktop_filter_auto').on('click', function () {
        pause = !pause;
        if (pause) {
            clearInterval(timer);
            $(this).html('Pausado');
        } else {
            startLoop();
            $(this).html('Auto');
        }
    });


    groups.on('change', function () {
        var options = $(this).children('option:selected');
        var vehicles = $('[data-vehicle]');
        if (options.length > 0) {
            vehicles.each(function () {
                $(this).hide();
                map.getTracker($(this).attr('data-vehicle')).hide();
            });
            options.each(function () {
                $('[data-groups~="' + $(this).text() + '"]').each(function () {
                    $(this).show();
                    map.getTracker($(this).attr('data-vehicle')).show();
                });
            });
        } else {
            vehicles.each(function () {
                $(this).show();
                map.getTracker($(this).attr('data-vehicle')).show();
            });
        }
        map.fit();
    });
});

$(window).load(function () {
    var active = getURLParameter('vehicle');
    if (active && map.getTracker(active)) {
        map.getTracker(active).fit();
    }
});