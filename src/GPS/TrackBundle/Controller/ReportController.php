<?php

namespace GPS\TrackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/report")
 */
class ReportController extends Controller
{
    /**
     * @Route("/vehicle", name="report_vehicle")
     * @Template()
     */
    public function vehicleAction()
    {
        $vehicles = $this->get('gps_track.vehiculo_manager')->findAllWithLastPlot(null, false);
        $vehicleGroups = $this->get('gps_track.vehiculo_manager')->getGroupNames(false);

        return array(
            'vehicles' => $vehicles,
            'vehicleGroups' => $vehicleGroups,
        );
    }

    /**
     * @Route("/alert", name="report_alert")
     * @Template()
     */
    public function alertAction()
    {
        return array();
    }

    /**
     * @Route("/driver", name="report_driver")
     * @Template()
     */
    public function driverAction()
    {
        return array();
    }

    /**
     * @Template()
     */
    public function filterAction()
    {
        $filterForm = $this->createFilterForm();

        return array(
            'filter' => $filterForm->createView(),
        );
    }

    private function createFilterForm()
    {
        $filterForm = $this->get('gps_track.report_filter.form.type');
        $form = $this->createForm($filterForm, array(), array(
            'action' => "#filter",
            'method' => 'GET',
        ));

        return $form;
    }
}
