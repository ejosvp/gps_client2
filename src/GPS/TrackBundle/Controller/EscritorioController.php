<?php

namespace GPS\TrackBundle\Controller;

use GPS\TrackBundle\Form\DesktopFilterType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/")
 */
class EscritorioController extends Controller
{
    /**
     * @Route("/", name="escritorio")
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction()
    {
        $date_string = $this->getRequest()->request->get('date', false);
        if ($date_string)
            $now = \DateTime::createFromFormat('Y-m-d H:i:s', $date_string);
        else
            $now = new \DateTime('-1 hour');

        $allPlots = $this->get('gps_track.plot_manager')->findAllPlots($now);
        foreach ($allPlots as $key => $plot) {
            $info = $this->renderView('GPSTrackBundle:Escritorio:resume.html.twig', array(
                'plot' => $plot
            ));
            $plot['info'] = $info;
            $allPlots[$plot['placa']][$plot['id']] = $plot;
            unset($allPlots[$key]);
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            return new JsonResponse(array(
                'allPlots' => $allPlots,
            ));
        } else {
            $vehicles = $this->get('gps_track.vehiculo_manager')->findAllWithLastPlot($now);

            $vehicleGroups = $this->get('gps_track.vehiculo_manager')->getGroupNames();

            $filterForm = $this->createFilterForm();

            return array(
                'vehicles' => $vehicles,
                'vehicleGroups' => $vehicleGroups,
                'allPlots' => $allPlots,
                'filterForm' => $filterForm->createView(),
            );
        }
    }

    private function createFilterForm()
    {
        $filterForm = $this->get('gps_track.desktop_filter.form.type');
        $form = $this->createForm($filterForm, array(), array(
            'action' => "#filter",
            'method' => 'GET',
        ));

        return $form;
    }
}
