<?php

namespace GPS\TrackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\TrackBundle\Entity\Vehiculo;
use GPS\TrackBundle\Form\VehiculoType;

/**
 * Vehiculo controller.
 *
 * @Route("/vehiculo")
 */
class VehiculoController extends Controller
{

    /**
     * Lists all Vehiculos.
     *
     * @Route("/", name="vehiculo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $vehiculos = $this->get('gps_track.vehiculo_manager')->findAll();

        return array(
            'vehiculos' => $vehiculos,
        );
    }
    /**
     * Creates a new Vehiculo.
     *
     * @Route("/", name="vehiculo_create")
     * @Method("POST")
     * @Template("GPSTrackBundle:Vehiculo:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $vehiculo = $this->get('gps_track.vehiculo_manager')->create();
        $form = $this->createCreateForm($vehiculo);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('gps_track.tracker_manager')->update($vehiculo);

            $this->setFlash('success', 'vehiculo.flash.created');

            return $this->redirect($this->generateUrl('vehiculo'));
        }

        return array(
            'vehiculo' => $vehiculo,
            'form' => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Vehiculo.
    *
    * @param Vehiculo $vehiculo The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Vehiculo $vehiculo)
    {
        $formType = $this->container->get('gps_track.vehiculo.form.type');
        $form = $this->createForm($formType, $vehiculo, array(
            'action' => $this->generateUrl('vehiculo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Crear',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Vehiculo.
     *
     * @Route("/new", name="vehiculo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $vehiculo = new Vehiculo();
        $form   = $this->createCreateForm($vehiculo);

        return array(
            'vehiculo' => $vehiculo,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Vehiculo.
     *
     * @Route("/{id}", name="vehiculo_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {

        $vehiculo = $this->get('gps_track.vehiculo_manager')->find($id);

        if (!$vehiculo) {
            throw $this->createNotFoundException('Unable to find Vehiculo.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'vehiculo' => $vehiculo,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Vehiculo.
     *
     * @Route("/{id}/edit", name="vehiculo_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $vehiculo = $this->get('gps_track.vehiculo_manager')->find($id);

        if (!$vehiculo) {
            throw $this->createNotFoundException('Unable to find Vehiculo.');
        }

        $form = $this->createEditForm($vehiculo);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'vehiculo' => $vehiculo,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Vehiculo.
    *
    * @param Vehiculo $vehiculo The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Vehiculo $vehiculo)
    {
        $formType = $this->container->get('gps_track.vehiculo.form.type');

        $form = $this->createForm($formType, $vehiculo, array(
            'action' => $this->generateUrl('vehiculo_update', array('id' => $vehiculo->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }
    /**
     * Edits an existing Vehiculo.
     *
     * @Route("/{id}", name="vehiculo_update")
     * @Method("PUT")
     * @Template("GPSTrackBundle:Vehiculo:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {

        $vehiculo = $this->get('gps_track.vehiculo_manager')->find($id);

        if (!$vehiculo) {
            throw $this->createNotFoundException('Unable to find Vehiculo.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $form = $this->createEditForm($vehiculo);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('gps_track.vehiculo_manager')->update($vehiculo);

            $this->setFlash('success', 'vehiculo.flash.updated');

            return $this->redirect($this->generateUrl('vehiculo'));
        }

        return array(
            'vehiculo' => $vehiculo,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Vehiculo.
     *
     * @Route("/{id}", name="vehiculo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $vehiculo = $this->get('gps_track.vehiculo_manager')->find($id);

            if (!$vehiculo) {
                throw $this->createNotFoundException('Unable to find Vehiculo.');
            }

            $this->get('gps_track.vehiculo_manager')->delete($vehiculo);

            $this->setFlash('success', 'vehiculo.flash.deleted');
        }

        return $this->redirect($this->generateUrl('vehiculo'));
    }

    /**
     * Creates a form to delete a Vehiculo by id.
     *
     * @param mixed $id The vehiculo id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('vehiculo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Eliminar',
                'attr' => array('class' => 'btn-danger')
            ))
            ->getForm()
        ;
    }

    /**
     * @param $action
     * @param $value
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->set(
            $action,
            $this->container->get('translator')->trans($value, [], 'GPSTrackBundle')
        );
    }
}
