<?php

namespace GPS\TrackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\TrackBundle\Entity\GeoZone;
use GPS\TrackBundle\Form\GeoZoneType;

/**
 * GeoZone controller.
 *
 * @Route("/geozone")
 */
class GeoZoneController extends Controller
{

    /**
     * Lists all GeoZones.
     *
     * @Route("/", name="geozone")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $geozones = $this->get('gps_track.geozone_manager')->findAll();

        return array(
            'geozones' => $geozones,
            'types' => GeoZone::$TYPES,
        );
    }
    /**
     * Creates a new GeoZone.
     *
     * @Route("/", name="geozone_create")
     * @Method("POST")
     * @Template("GPSTrackBundle:GeoZone:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $geozone = $this->get('gps_track.geozone_manager')->create();
        $form = $this->createCreateForm($geozone);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('gps_track.geozone_manager')->update($geozone);

            return $this->redirect($this->generateUrl('geozone'));
        }

        return array(
            'geozone' => $geozone,
            'form' => $form->createView(),
        );
    }

    /**
    * Creates a form to create a GeoZone.
    *
    * @param GeoZone $geozone The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(GeoZone $geozone)
    {
        $form = $this->createForm(new GeoZoneType(), $geozone, array(
            'action' => $this->generateUrl('geozone_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Crear',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new GeoZone.
     *
     * @Route("/new", name="geozone_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $geozone = $this->get('gps_track.geozone_manager')->create();
        $form   = $this->createCreateForm($geozone);

        return array(
            'geozone' => $geozone,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing GeoZone.
     *
     * @Route("/{id}/edit", name="geozone_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $geozone = $this->get('gps_track.geozone_manager')->find($id);

        if (!$geozone) {
            throw $this->createNotFoundException('Unable to find GeoZone.');
        }

        $form = $this->createEditForm($geozone);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'geozone' => $geozone,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a GeoZone.
    *
    * @param GeoZone $geozone The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(GeoZone $geozone)
    {
        $form = $this->createForm(new GeoZoneType(), $geozone, array(
            'action' => $this->generateUrl('geozone_update', array('id' => $geozone->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }
    /**
     * Edits an existing GeoZone.
     *
     * @Route("/{id}", name="geozone_update")
     * @Method("PUT")
     * @Template("GPSTrackBundle:GeoZone:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $geozone = $this->get('gps_track.geozone_manager')->find($id);

        if (!$geozone) {
            throw $this->createNotFoundException('Unable to find GeoZone.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $form = $this->createEditForm($geozone);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('gps_track.geozone_manager')->update($geozone);

            return $this->redirect($this->generateUrl('geozone'));
        }

        return array(
            'geozone' => $geozone,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a GeoZone.
     *
     * @Route("/{id}", name="geozone_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $geozone = $this->get('gps_track.geozone_manager')->find($id);

            if (!$geozone) {
                throw $this->createNotFoundException('Unable to find GeoZone.');
            }

            $this->get('gps_track.geozone_manager')->delete($geozone);
        }

        return $this->redirect($this->generateUrl('geozone'));
    }

    /**
     * Creates a form to delete a GeoZone by id.
     *
     * @param mixed $id The geozone id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('geozone_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Eliminar',
                'attr' => array('class' => 'btn-danger')
            ))
            ->getForm()
        ;
    }
}
