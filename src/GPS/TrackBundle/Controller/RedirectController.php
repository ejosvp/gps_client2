<?php

namespace GPS\TrackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class RedirectController extends Controller
{
    public function homepageAction()
    {
        $opciones = $this->getUser()->getOpciones();
        $homepage = $opciones['homepage'];

        try {
            $homepage = $this->generateUrl($homepage);
        } catch (RouteNotFoundException $e) {
            $this->get('session')->getFlashBag()->set('danger',
                sprintf('Página de inicio no valida, puede cambiar en <a class="alert-link" href="%s">Editar Configuración</a>',
                    $this->generateUrl('fos_user_profile_edit')
                ));
            $homepage = $this->generateUrl('escritorio');
        }

        return $this->redirect($homepage);
    }
}
