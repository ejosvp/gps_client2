<?php

namespace GPS\TrackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\TrackBundle\Entity\Cliente;
use GPS\TrackBundle\Form\ClienteProfileType;

/**
 * ClientProfile controller.
 *
 * @Route("/cliente_profile")
 */
class ClienteProfileController extends Controller
{

    /**
     * Client profile.
     *
     * @Route("/edit", name="cliente_profile_edit")
     * @Method("GET|PUT")
     * @Template()
     */
    public function editAction()
    {
        $client = $this->get('gps.client_context')->getCurrent();

        $form   = $this->createProfileForm($client);

        if ('PUT' === $this->getRequest()->getMethod()) {
            $form->handleRequest($this->getRequest());

            if ($form->isValid()) {
                $this->get('gps_track.cliente_manager')->update($client);

                $this->setFlash('success', 'cliente_profile.flash.updated');

                return $this->redirect($this->generateUrl('cliente_profile_edit'));
            }
        }

        return array(
            'client' => $client,
            'form' => $form->createView(),
        );
    }

    private function createProfileForm(Cliente $cliente)
    {
        $form = $this->createForm(new ClienteProfileType, $cliente, array(
            'action' => $this->generateUrl('cliente_profile_edit'),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }

    /**
     * @param $action
     * @param $value
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->set(
            $action,
            $this->container->get('translator')->trans($value, [], 'GPSTrackBundle')
        );
    }
}
