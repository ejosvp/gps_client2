<?php

namespace GPS\TrackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\TrackBundle\Entity\Chofer;
use GPS\TrackBundle\Form\ChoferType;

/**
 * Chofer controller.
 *
 * @Route("/chofer")
 */
class ChoferController extends Controller
{

    /**
     * Lists all Choferes.
     *
     * @Route("/", name="chofer")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $choferes = $this->get('gps_track.chofer_manager')->findAll();

        return array(
            'choferes' => $choferes,
        );
    }
    /**
     * Creates a new Chofer.
     *
     * @Route("/", name="chofer_create")
     * @Method("POST")
     * @Template("GPSTrackBundle:Chofer:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $chofer = $this->get('gps_track.chofer_manager')->create();
        $form = $this->createCreateForm($chofer);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('gps_track.chofer_manager')->update($chofer);

            $this->setFlash('success', 'chofer.flash.created');

            return $this->redirect($this->generateUrl('chofer'));
        }

        return array(
            'chofer' => $chofer,
            'form' => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Chofer.
    *
    * @param Chofer $chofer The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Chofer $chofer)
    {
        $form = $this->createForm(new ChoferType(), $chofer, array(
            'action' => $this->generateUrl('chofer_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Crear',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Chofer.
     *
     * @Route("/new", name="chofer_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $chofer = $this->get('gps_track.chofer_manager')->create();
        $form   = $this->createCreateForm($chofer);

        return array(
            'chofer' => $chofer,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Chofer.
     *
     * @Route("/{id}/edit", name="chofer_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $chofer = $this->get('gps_track.chofer_manager')->find($id);

        if (!$chofer) {
            throw $this->createNotFoundException('Unable to find Chofer.');
        }

        $form = $this->createEditForm($chofer);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'chofer' => $chofer,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Chofer.
    *
    * @param Chofer $chofer The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Chofer $chofer)
    {
        $form = $this->createForm(new ChoferType(), $chofer, array(
            'action' => $this->generateUrl('chofer_update', array('id' => $chofer->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }
    /**
     * Edits an existing Chofer.
     *
     * @Route("/{id}", name="chofer_update")
     * @Method("PUT")
     * @Template("GPSTrackBundle:Chofer:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $chofer = $this->get('gps_track.chofer_manager')->find($id);

        if (!$chofer) {
            throw $this->createNotFoundException('Unable to find Chofer.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $form = $this->createEditForm($chofer);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('gps_track.chofer_manager')->update($chofer);

            $this->setFlash('success', 'chofer.flash.updated');

            return $this->redirect($this->generateUrl('chofer'));
        }

        return array(
            'chofer' => $chofer,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Chofer.
     *
     * @Route("/{id}", name="chofer_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $chofer = $this->get('gps_track.chofer_manager')->find($id);

            if (!$chofer) {
                throw $this->createNotFoundException('Unable to find Chofer.');
            }

            $this->get('gps_track.chofer_manager')->delete($chofer);

            $this->setFlash('success', 'chofer.flash.deleted');
        }

        return $this->redirect($this->generateUrl('chofer'));
    }

    /**
     * Creates a form to delete a Chofer by id.
     *
     * @param mixed $id The chofer id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('chofer_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Eliminar',
                'attr' => array('class' => 'btn-danger')
            ))
            ->getForm()
        ;
    }

    /**
     * @param $action
     * @param $value
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->set(
            $action,
            $this->container->get('translator')->trans($value, [], 'GPSTrackBundle')
        );
    }
}
