<?php

namespace GPS\TrackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\TrackBundle\Entity\Tracker;
use GPS\TrackBundle\Form\TrackerType;

/**
 * Tracker controller.
 *
 * @Route("/tracker")
 */
class TrackerController extends Controller
{

    /**
     * Lists all Trackers.
     *
     * @Route("/", name="tracker")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $trackers = $this->get('gps_track.tracker_manager')->findAll();

        return array(
            'trackers' => $trackers,
        );
    }
    /**
     * Creates a new Tracker.
     *
     * @Route("/", name="tracker_create")
     * @Method("POST")
     * @Template("GPSTrackBundle:Tracker:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $tracker = $this->get('gps_track.tracker_manager')->create();
        $form = $this->createCreateForm($tracker);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('gps_track.tracker_manager')->update($tracker);

            $this->setFlash('success', 'tracker.flash.created');

            return $this->redirect($this->generateUrl('tracker'));
        }

        return array(
            'tracker' => $tracker,
            'form' => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Tracker.
    *
    * @param Tracker $tracker The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Tracker $tracker)
    {
        $form = $this->createForm(new TrackerType(), $tracker, array(
            'action' => $this->generateUrl('tracker_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Crear',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Tracker.
     *
     * @Route("/new", name="tracker_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $tracker = $this->get('gps_track.tracker_manager')->create();
        $form   = $this->createCreateForm($tracker);

        return array(
            'tracker' => $tracker,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Tracker.
     *
     * @Route("/{id}/edit", name="tracker_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $tracker = $this->get('gps_track.tracker_manager')->find($id);

        if (!$tracker) {
            throw $this->createNotFoundException('Unable to find Tracker.');
        }

        $form = $this->createEditForm($tracker);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'tracker' => $tracker,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Tracker.
    *
    * @param Tracker $tracker The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Tracker $tracker)
    {
        $form = $this->createForm(new TrackerType(), $tracker, array(
            'action' => $this->generateUrl('tracker_update', array('id' => $tracker->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }
    /**
     * Edits an existing Tracker.
     *
     * @Route("/{id}", name="tracker_update")
     * @Method("PUT")
     * @Template("GPSTrackBundle:Tracker:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $tracker = $this->get('gps_track.tracker_manager')->find($id);

        if (!$tracker) {
            throw $this->createNotFoundException('Unable to find Tracker.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $form = $this->createEditForm($tracker);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('gps_track.tracker_manager')->update($tracker);

            $this->setFlash('success', 'tracker.flash.updated');

            return $this->redirect($this->generateUrl('tracker'));
        }

        return array(
            'tracker' => $tracker,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Tracker.
     *
     * @Route("/{id}", name="tracker_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $tracker = $this->get('gps_track.tracker_manager')->find($id);

            if (!$tracker) {
                throw $this->createNotFoundException('Unable to find Tracker.');
            }

            $this->get('gps_track.tracker_manager')->delete($tracker);

            $this->setFlash('success', 'tracker.flash.deleted');
        }

        return $this->redirect($this->generateUrl('tracker'));
    }

    /**
     * Creates a form to delete a Tracker by id.
     *
     * @param mixed $id The tracker id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tracker_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Eliminar',
                'attr' => array('class' => 'btn-danger')
            ))
            ->getForm()
        ;
    }

    /**
     * @param $action
     * @param $value
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->set(
            $action,
            $this->container->get('translator')->trans($value, [], 'GPSTrackBundle')
        );
    }
}
