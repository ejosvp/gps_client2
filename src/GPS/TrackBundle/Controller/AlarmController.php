<?php

namespace GPS\TrackBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\TrackBundle\Entity\Alarm;
use GPS\TrackBundle\Form\AlarmType;

/**
 * Alarm controller.
 *
 * @Route("/alarm")
 */
class AlarmController extends Controller
{

    /**
     * Lists all Alarms.
     *
     * @Route("/", name="alarm")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $alarms = $em->getRepository('GPSTrackBundle:Alarm')->findAll();

        return array(
            'alarms' => $alarms,
        );
    }
    /**
     * Creates a new Alarm.
     *
     * @Route("/", name="alarm_create")
     * @Method("POST")
     * @Template("GPSTrackBundle:Alarm:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $alarm = new Alarm();
        $form = $this->createCreateForm($alarm);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($alarm);
            $em->flush();

            return $this->redirect($this->generateUrl('alarm'));
        }

        return array(
            'alarm' => $alarm,
            'form' => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Alarm.
    *
    * @param Alarm $alarm The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Alarm $alarm)
    {
        $form = $this->createForm(new AlarmType(), $alarm, array(
            'action' => $this->generateUrl('alarm_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Crear',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Alarm.
     *
     * @Route("/new", name="alarm_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $alarm = new Alarm();
        $form   = $this->createCreateForm($alarm);

        return array(
            'alarm' => $alarm,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Alarm.
     *
     * @Route("/{id}", name="alarm_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $alarm = $em->getRepository('GPSTrackBundle:Alarm')->find($id);

        if (!$alarm) {
            throw $this->createNotFoundException('Unable to find Alarm.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'alarm' => $alarm,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Alarm.
     *
     * @Route("/{id}/edit", name="alarm_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $alarm = $em->getRepository('GPSTrackBundle:Alarm')->find($id);

        if (!$alarm) {
            throw $this->createNotFoundException('Unable to find Alarm.');
        }

        $form = $this->createEditForm($alarm);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'alarm' => $alarm,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Alarm.
    *
    * @param Alarm $alarm The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Alarm $alarm)
    {
        $form = $this->createForm(new AlarmType(), $alarm, array(
            'action' => $this->generateUrl('alarm_update', array('id' => $alarm->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }
    /**
     * Edits an existing Alarm.
     *
     * @Route("/{id}", name="alarm_update")
     * @Method("PUT")
     * @Template("GPSTrackBundle:Alarm:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $alarm = $em->getRepository('GPSTrackBundle:Alarm')->find($id);

        if (!$alarm) {
            throw $this->createNotFoundException('Unable to find Alarm.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $form = $this->createEditForm($alarm);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->update($alarm);
            $em->flush();

            return $this->redirect($this->generateUrl('alarm_edit', array('id' => $id)));
        }

        return array(
            'alarm' => $alarm,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Alarm.
     *
     * @Route("/{id}", name="alarm_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $alarm = $em->getRepository('GPSTrackBundle:Alarm')->find($id);

            if (!$alarm) {
                throw $this->createNotFoundException('Unable to find Alarm.');
            }

            $em->remove($alarm);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('alarm'));
    }

    /**
     * Creates a form to delete a Alarm by id.
     *
     * @param mixed $id The alarm id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('alarm_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Eliminar',
                'attr' => array('class' => 'btn-danger')
            ))
            ->getForm()
        ;
    }
}
