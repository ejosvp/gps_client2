<?php

namespace GPS\TrackBundle\Twig;

use GPS\TrackBundle\Security\ClientContext;
use GPS\TrackBundle\Util\PlotDecoder;

class GPSTrackExtension extends \Twig_Extension
{
    /** @var  ClientContext */
    private $clientContext;

    public function __construct(ClientContext $clientContext)
    {
        $this->clientContext = $clientContext;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('to_bin', array($this, 'toBin', array('is_safe' => array('html')))),
            new \Twig_SimpleFilter('date_diff', array($this, 'dateDiff', array('is_safe' => array('html')))),
        );
    }

    public function getGlobals()
    {
        return array(
            'current_client' => $this->clientContext->getCurrent(),
        );
    }

    public function toBin($n)
    {
        return PlotDecoder::hexToBin($n);
    }

    public function dateDiff($date1, $date2, $format = '%R%a días')
    {
        if (is_string($date1)) $date1 = new \DateTime($date1);
        if (is_string($date2)) $date2 = new \DateTime($date2);

        $interval = $date1->diff($date2);
        return $interval->format($format);
    }

    public function getName()
    {
        return 'gps_track.twig.extension.gps_track';
    }
}