<?php

namespace GPS\TrackBundle\EventListener;

use Doctrine\ORM\EntityManager;
use GPS\TrackBundle\Entity\Cliente;
use GPS\TrackBundle\Security\ClientContext;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RequestContextAwareInterface;
use Symfony\Component\Routing\RouterInterface;

class CurrentClientListener implements EventSubscriberInterface
{
    /** @var  EntityManager */
    private $em;

    /** @var  ClientContext */
    private $cm;

    /** @var RouterInterface */
    private $matcher;

    /** @var  RequestContext */
    private $context;

    private $baseHost;

    private $request;

    public function __construct(EntityManager $em, ClientContext $cm, RouterInterface $matcher, RequestContext $context = null, $baseHost)
    {
        if (!$matcher instanceof UrlMatcherInterface && !$matcher instanceof RequestMatcherInterface) {
            throw new \InvalidArgumentException('Matcher must either implement UrlMatcherInterface or RequestMatcherInterface.');
        }

        if (null === $context && !$matcher instanceof RequestContextAwareInterface) {
            throw new \InvalidArgumentException('You must either pass a RequestContext or the matcher must implement RequestContextAwareInterface.');
        }

        $this->matcher = $matcher;
        $this->context = $context ? : $matcher->getContext();

        $this->em = $em;
        $this->cm = $cm;

        $this->baseHost = $baseHost;
    }

    public function setRequest(Request $request = null)
    {
        if (null !== $request && $this->request !== $request) {
            $this->context->fromRequest($request);
        }
        $this->request = $request;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        $this->setRequest($request);

        if ($request->attributes->has('_controller')) {
            return;
        }

        $currentHost = $request->getHttpHost();
        $subdomain = str_replace('.' . $this->baseHost, '', $currentHost);

        if ($subdomain != $currentHost) {
            /** @var Cliente $cliente */
            $cliente = $this->em->getRepository('GPSTrackBundle:Cliente')->findOneBy(array('nombre' => $subdomain));

            if (!$cliente) {
                $event->setResponse(new RedirectResponse($this->matcher->generate('gps_site_services')), 404);
            } elseif (!$cliente->getActivo()) {
                $event->setResponse(new RedirectResponse($this->matcher->generate('gps_track_disabled')), 403);
            } else {

                $this->cm->setCurrent($cliente);
                $this->context->setParameter('cliente', $cliente->getNombre());
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => array(array('onKernelRequest', 33)),
        );
    }
}