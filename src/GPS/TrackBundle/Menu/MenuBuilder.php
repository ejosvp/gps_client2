<?php

namespace GPS\TrackBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

class MenuBuilder
{
    /** @var \Knp\Menu\MenuFactory */
    private $factory;

    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMainMenu(Request $request, SecurityContext $securityContext)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');

        if ($securityContext->isGranted('ROLE_DESKTOP')) {
            $menu->addChild('Escritorio', array('route' => 'escritorio'));
        }
        if ($securityContext->isGranted('ROLE_TRACKER')) {
            $menu->addChild('Trackers', array('route' => 'tracker'));
        }
        if ($securityContext->isGranted('ROLE_DRIVER')) {
            $menu->addChild('Choferes', array('route' => 'chofer'));
        }
        if ($securityContext->isGranted('ROLE_VEHICLE')) {
            $menu->addChild('Vehiculos', array('route' => 'vehiculo'));
        }
        if ($securityContext->isGranted('ROLE_GEOZONE')) {
            $menu->addChild('GeoZonas', array('route' => 'geozone'));
        }
        if ($securityContext->isGranted('ROLE_USER')) {
            $menu->addChild('Usuarios', array('route' => 'usuario'));
        }
        if ($securityContext->isGranted('ROLE_GROUP')) {
            $menu->addChild('Grupos', array('route' => 'fos_user_group_list'));
        }

        if ($securityContext->isGranted('ROLE_REPORT')) {
            $reports = $menu->addChild(
                '<span class="glyphicon glyphicon-list"></span> Reportes <b class="caret"></b>',
                array('uri' => '#'));
            $reports->setChildrenAttribute('class', 'dropdown-menu');
            $reports->setAttribute('class', 'dropdown');
            $reports->setExtra('safe_label', true);
            $reports->setLinkAttributes(array(
                'class' => 'dropdown-toggle',
                'data-toggle' => 'dropdown',
            ));

            $reports->addChild('Vehiculos',     array('route' => 'report_vehicle'));
            $reports->addChild('Alertas',       array('route' => 'report_alert'));
            $reports->addChild('Rendimiento',   array('route' => 'report_driver'));
        }

        return $menu;
    }

    public function createUserMenu(Request $request, SecurityContext $securityContext)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

        if ($securityContext->isGranted('ROLE_ADMIN')) {
            $menu->addChild(
                '<span class="glyphicon glyphicon-tower"></span> Cliente'
                , array('route' => 'cliente_profile_edit'))->setExtra('safe_label', true);
        }

        $user = $menu->addChild(
            '<span class="glyphicon glyphicon-user"></span> '
            . $securityContext->getToken()->getUsername()
            . ' <b class="caret"></b>',
            array('uri' => '#'));
        $user->setChildrenAttribute('class', 'dropdown-menu');
        $user->setAttribute('class', 'dropdown');
        $user->setExtra('safe_label', true);
        $user->setLinkAttributes(array(
            'class' => 'dropdown-toggle',
            'data-toggle' => 'dropdown',
        ));

        $user->addChild('Editar Información', array('route' => 'fos_user_profile_edit'));
        $user->addChild('Cambiar Contraseña', array('route' => 'fos_user_change_password'));
        $user->addChild('Salir', array('route' => 'fos_user_security_logout'));

        return $menu;
    }

    public function createBackMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');

        $referer = $request->headers->get('referer');
        if ($referer) {
            $menu->addChild('Volver', array('uri' => $referer));
        }

        return $menu;
    }
}