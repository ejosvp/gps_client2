<?php

namespace GPS\TrackBundle\Security;

use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Security\UserProvider;

class ClientUserProvider extends UserProvider
{
    /**
     * @var ClientContext
     */
    protected $clientManager;

    /**
     * Constructor.
     *
     * @param UserManagerInterface $userManager
     * @param ClientContext $clientManager
     */
    public function __construct(UserManagerInterface $userManager, ClientContext $clientManager)
    {
        $this->userManager = $userManager;
        $this->clientManager = $clientManager;
    }

    /**
     * {@inheritDoc}
     */
    protected function findUser($username)
    {
        return $this->userManager->findUserBy([
            'username' => $username,
            'cliente' => $this->clientManager->getCurrent()
        ]);
    }
}
