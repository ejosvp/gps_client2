<?php

namespace GPS\TrackBundle\Security;


use GPS\TrackBundle\Entity\Cliente;

class ClientContext
{
    /** @var Cliente */
    private $current;

    /**
     * @return Cliente
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param $current
     */
    public function setCurrent($current)
    {
        $this->current = $current;
    }
}