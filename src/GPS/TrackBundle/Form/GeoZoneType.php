<?php

namespace GPS\TrackBundle\Form;

use GPS\TrackBundle\Entity\GeoZone;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GeoZoneType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'label' => 'form.name', 'translation_domain' => 'GPSTrackBundle',
            ))
            ->add('type', 'choice', array(
                'label' => 'form.type', 'translation_domain' => 'GPSTrackBundle',
                'choices' => GeoZone::$TYPES,
                'empty_value' => ''
            ))
            ->add('color', null, array(
                'label' => 'form.color', 'translation_domain' => 'GPSTrackBundle',
            ))
            ->add('serialized', 'hidden')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPS\TrackBundle\Entity\GeoZone'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gps_trackbundle_geozone';
    }
}
