<?php

namespace GPS\TrackBundle\Form;

use FOS\UserBundle\Model\GroupManagerInterface;
use GPS\UserBundle\Doctrine\GroupManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class DesktopFilterType extends AbstractType
{
    /** @var GroupManager */
    private $groupManager;

    public function __construct(GroupManagerInterface $groupManager)
    {
        $this->groupManager = $groupManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('groups', 'entity', array(
                'class' => 'GPSUserBundle:Grupo',
                'query_builder' => $this->groupManager->getCurrentUserGroupsQueryBuilder(),
                'multiple' => true,
                'attr' => array('class' => 'chosen'),
                'required' => false,
                'label' => 'Grupos',
            ))
            ->add('center', 'button', array(
                'label' => 'Centrar',
                'attr' => array('class' => 'btn-xs btn-default')
            ))
            ->add('auto', 'button', array(
                'label' => 'Auto',
                'attr' => array('class' => 'btn-xs btn-success')
            ))
            ->add('reset', 'button', array(
                'label' => 'Reset',
                'attr' => array('class' => 'btn-xs btn-danger')
            ));
    }

    public function getName()
    {
        return 'desktop_filter';
    }
}
