<?php

namespace GPS\TrackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChoferType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellido')
            ->add('dni')
            ->add('licencia')
            ->add('licenciaExpira', 'collot_datetime', array(
                'pickerOptions' => array(
                    'format' => 'mm/dd/yyyy',
                    'autoclose' => true,
                    'startView' => 'month',
                    'minView' => 'decade',
                    'maxView' => 'decade',
                    'language' => 'es',
                ),
                'label' => "Expiración de Licencia",
            ))
            ->add('licenciaTipo', null, array(
                'label' => 'Tipo de Licencia'
            ))
            ->add('tipoSangre');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPS\TrackBundle\Entity\Chofer'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gps_trackbundle_chofer';
    }
}
