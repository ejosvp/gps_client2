<?php

namespace GPS\TrackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use GPS\TrackBundle\Security\ClientContext;
use Doctrine\ORM\EntityRepository;
use GPS\TrackBundle\Entity\Tracker;

class VehiculoType extends AbstractType
{
    /** @var ClientContext */
    protected $clientContext;

    public function __construct(ClientContext $clientContext)
    {
        $this->clientContext = $clientContext;
    }

     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('placa')
            ->add('marca')
            ->add('modelo')
            ->add('anho', null, array(
                'label' => "Año",
            ))
            ->add('color')
            ->add('seguro')
            ->add('seguroExpira', 'collot_datetime', array( 'pickerOptions' =>
            array('format' => 'mm/dd/yyyy',
                'autoclose' => true,
                'startView' => 'month',
                'minView' => 'decade',
                'maxView' => 'decade',
                'language' => 'es',
            ),
                'label' => "Expiración del seguro",
            ))
            ->add('seguroConpania', null, array(
                'label' => "Compañia de seguros",
            ))
            ->add('foto')
            ->add('icono')
            ->add('tracker', 'entity', array(
                'class' => 'GPSTrackBundle:Tracker',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->select('t, v')
                        ->leftJoin('t.vehiculo', 'v')
                        ->where('t.cliente = :cliente')
                        ->andWhere('v.tracker IS NULL')
                        ->setParameter('cliente', $this->clientContext->getCurrent());
                },
            ))
            ->add('chofer', 'entity', array(
                'class' => 'GPSTrackBundle:Chofer',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.cliente = :cliente')
                        ->setParameter('cliente', $this->clientContext->getCurrent());
                },
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GPS\TrackBundle\Entity\Vehiculo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gps_trackbundle_vehiculo';
    }
}
