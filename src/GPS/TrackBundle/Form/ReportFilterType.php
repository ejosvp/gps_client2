<?php

namespace GPS\TrackBundle\Form;

use FOS\UserBundle\Model\GroupManagerInterface;
use GPS\UserBundle\Doctrine\GroupManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ReportFilterType extends AbstractType
{
    /** @var GroupManager */
    private $groupManager;

    public function __construct(GroupManagerInterface $groupManager)
    {
        $this->groupManager = $groupManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('groups', 'entity', array(
                'class' => 'GPSUserBundle:Grupo',
                'query_builder' => $this->groupManager->getQueryBuilder(),
                'multiple' => true,
                'attr' => array('class' => 'chosen'),
                'required' => false,
                'label' => 'Grupos',
            ));
    }

    public function getName()
    {
        return 'report_filter';
    }
}
