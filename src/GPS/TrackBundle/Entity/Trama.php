<?php

namespace GPS\TrackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trama
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Trama
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Tracker
     *
     * @ORM\ManyToOne(targetEntity="Tracker", inversedBy="tramas")
     */
    private $tracker;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="serverTime", type="datetime")
     */
    private $serverTime;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="valid", type="boolean")
     */
    private $valid;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="decimal", precision=10, scale=4)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="decimal", precision=10, scale=4)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="altitude", type="decimal", precision=10, scale=4)
     */
    private $altitude;

    /**
     * @var float
     *
     * @ORM\Column(name="speed", type="decimal", precision=4, scale=1)
     */
    private $speed;

    /**
     * @var float
     *
     * @ORM\Column(name="course", type="decimal", precision=4, scale=1)
     */
    private $course;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var array
     *
     * @ORM\Column(name="extendedInfo", type="array")
     */
    private $extendedInfo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tracker
     *
     * @param Tracker $tracker
     * @return Trama
     */
    public function setTracker(Tracker $tracker)
    {
        $this->tracker = $tracker;
        $tracker->addTrama($this);
    
        return $this;
    }

    /**
     * Get tracker
     *
     * @return Tracker
     */
    public function getTracker()
    {
        return $this->tracker;
    }

    /**
     * @param \Datetime $serverTime
     */
    public function setServerTime($serverTime)
    {
        $this->serverTime = $serverTime;
    }

    /**
     * @return \Datetime
     */
    public function getServerTime()
    {
        return $this->serverTime;
    }

    /**
     * @param \Datetime $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return \Datetime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param boolean $valid
     */
    public function setValid($valid)
    {
        $this->valid = $valid;
    }

    /**
     * @return boolean
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param float $altitude
     */
    public function setAltitude($altitude)
    {
        $this->altitude = $altitude;
    }

    /**
     * @return float
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * @param float $speed
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }

    /**
     * @return float
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param float $course
     */
    public function setCourse($course)
    {
        $this->course = $course;
    }

    /**
     * @return float
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param array $extendedInfo
     */
    public function setExtendedInfo($extendedInfo)
    {
        $this->extendedInfo = $extendedInfo;
    }

    /**
     * @return array
     */
    public function getExtendedInfo()
    {
        return $this->extendedInfo;
    }

}
