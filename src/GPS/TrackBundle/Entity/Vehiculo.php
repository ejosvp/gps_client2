<?php

namespace GPS\TrackBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\GroupInterface;
use GPS\UserBundle\Entity\Usuario;

/**
 * Vehiculo
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Vehiculo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="placa", type="string", length=255)
     */
    private $placa;

    /**
     * @var string
     *
     * @ORM\Column(name="marca", type="string", length=255, nullable=true)
     */
    private $marca;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo", type="string", length=255, nullable=true)
     */
    private $modelo;

    /**
     * @var string
     *
     * @ORM\Column(name="anho", type="string", length=255, nullable=true)
     */
    private $anho;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="seguro", type="string", length=255)
     */
    private $seguro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="seguro_expira", type="date")
     */
    private $seguroExpira;

    /**
     * @var string
     *
     * @ORM\Column(name="seguro_conpania", type="string", length=255)
     */
    private $seguroConpania;

    /**
     * @var string
     *
     * @ORM\Column(name="foto", type="string", length=255, nullable=true)
     */
    private $foto;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="icono", type="object")
     */
    private $icono;

    /**
     * @var Tracker
     *
     * @ORM\OneToOne(targetEntity="Tracker", inversedBy="vehiculo")
     * @ORM\JoinColumn(name="tracker_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $tracker;

    /**
     * @var Chofer
     *
     * @ORM\ManyToOne(targetEntity="Chofer")
     * @ORM\JoinColumn(name="chofer_id", referencedColumnName="id", nullable=true)
     */
    private $chofer;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="GPS\UserBundle\Entity\Grupo", mappedBy="vehiculos")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=false)
     */
    private $groups;

    /**
     * @var Cliente
     *
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id", nullable=false)
     */
    private $cliente;


    public function __toString()
    {
        return (string) $this->getPlaca();
    }

    public function getTrackerPlaca()
    {
        return sprintf('%s (%s)', $this->getTracker(), $this->getPlaca());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set placa
     *
     * @param string $placa
     * @return Vehiculo
     */
    public function setPlaca($placa)
    {
        $this->placa = $placa;
    
        return $this;
    }

    /**
     * Get placa
     *
     * @return string 
     */
    public function getPlaca()
    {
        return $this->placa;
    }

    /**
     * Set marca
     *
     * @param string $marca
     * @return Vehiculo
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;
    
        return $this;
    }

    /**
     * Get marca
     *
     * @return string 
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     * @return Vehiculo
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    
        return $this;
    }

    /**
     * Get modelo
     *
     * @return string 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set anho
     *
     * @param string $anho
     * @return Vehiculo
     */
    public function setAnho($anho)
    {
        $this->anho = $anho;
    
        return $this;
    }

    /**
     * Get anho
     *
     * @return string 
     */
    public function getAnho()
    {
        return $this->anho;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Vehiculo
     */
    public function setColor($color)
    {
        $this->color = $color;
    
        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set seguro
     *
     * @param string $seguro
     * @return Vehiculo
     */
    public function setSeguro($seguro)
    {
        $this->seguro = $seguro;
    
        return $this;
    }

    /**
     * Get seguro
     *
     * @return string 
     */
    public function getSeguro()
    {
        return $this->seguro;
    }

    /**
     * Set seguroExpira
     *
     * @param \DateTime $seguroExpira
     * @return Vehiculo
     */
    public function setSeguroExpira($seguroExpira)
    {
        $this->seguroExpira = $seguroExpira;
    
        return $this;
    }

    /**
     * Get seguroExpira
     *
     * @return \DateTime
     */
    public function getSeguroExpira()
    {
        return $this->seguroExpira;
    }

    /**
     * Set seguroConpania
     *
     * @param string $seguroConpania
     * @return Vehiculo
     */
    public function setSeguroConpania($seguroConpania)
    {
        $this->seguroConpania = $seguroConpania;
    
        return $this;
    }

    /**
     * Get seguroConpania
     *
     * @return string 
     */
    public function getSeguroConpania()
    {
        return $this->seguroConpania;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return Vehiculo
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    
        return $this;
    }

    /**
     * Get foto
     *
     * @return string 
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set icono
     *
     * @param \stdClass $icono
     * @return Vehiculo
     */
    public function setIcono($icono)
    {
        $this->icono = $icono;

        return $this;
    }

    /**
     * Get icono
     *
     * @return \stdClass
     */
    public function getIcono()
    {
        return $this->icono;
    }

    /**
     * Set tracker
     *
     * @param Tracker $tracker
     * @return Vehiculo
     */
    public function setTracker(Tracker $tracker)
    {
        $this->tracker = $tracker;
        $tracker->setVehiculo($this);
    
        return $this;
    }

    /**
     * Get tracker
     *
     * @return Tracker
     */
    public function getTracker()
    {
        return $this->tracker;
    }

    /**
     * Set chofer
     *
     * @param Chofer $chofer
     * @return Vehiculo
     */
    public function setChofer(Chofer $chofer)
    {
        $this->chofer = $chofer;

        return $this;
    }

    /**
     * Get chofer
     *
     * @return Chofer
     */
    public function getChofer()
    {
        return $this->chofer;
    }

    /**
     * Set groups
     *
     * @param ArrayCollection $groups
     * @return Vehiculo
     */
    public function setGroups(ArrayCollection $groups)
    {
        $this->groups = $groups;
    
        return $this;
    }

    /**
     * Get groups
     *
     * @return ArrayCollection
     */
    public function getGroups()
    {
        return $this->groups ?: $this->groups = new ArrayCollection();
    }

    public function getGroupNames()
    {
        $names = array();
        foreach ($this->getGroups() as $group) {
            $names[] = $group->getName();
        }

        return $names;
    }

    public function hasGroup($name)
    {
        return in_array($name, $this->getGroupNames());
    }

    public function addGroup(GroupInterface $group)
    {
        if (!$this->getGroups()->contains($group)) {
            $this->getGroups()->add($group);
        }

        return $this;
    }

    public function removeGroup(GroupInterface $group)
    {
        if ($this->getGroups()->contains($group)) {
            $this->getGroups()->removeElement($group);
        }

        return $this;
    }

    /**
     * Set cliente
     *
     * @param Cliente $cliente
     * @return Vehiculo
     */
    public function setCliente(Cliente $cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }
}
