<?php

namespace GPS\TrackBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Tracker
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Tracker
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=255)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="marca", type="string", length=255, nullable=true)
     */
    private $marca;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo", type="string", length=255, nullable=true)
     */
    private $modelo;

    /**
     * @var string
     *
     * @ORM\Column(name="imei", type="string", length=255, nullable=true)
     */
    private $imei;

    /**
     * @var string
     *
     * @ORM\Column(name="chip", type="string", length=255, nullable=true)
     */
    private $chip;

    /**
     * @var Vehiculo
     *
     * @ORM\OneToOne(targetEntity="Vehiculo", mappedBy="tracker")
     */
    private $vehiculo;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Trama", mappedBy="tracker")
     */
    private $tramas;

    /**
     * @var Cliente
     *
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id", nullable=false)
     */
    private $cliente;


    public function __construct()
    {
        $this->tramas = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getChip();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Tracker
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set marca
     *
     * @param string $marca
     * @return Tracker
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;
    
        return $this;
    }

    /**
     * Get marca
     *
     * @return string 
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     * @return Tracker
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    
        return $this;
    }

    /**
     * Get modelo
     *
     * @return string 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set imei
     *
     * @param string $imei
     * @return Tracker
     */
    public function setImei($imei)
    {
        $this->imei = $imei;
    
        return $this;
    }

    /**
     * Get imei
     *
     * @return string 
     */
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * Set chip
     *
     * @param string $chip
     * @return Tracker
     */
    public function setChip($chip)
    {
        $this->chip = $chip;
    
        return $this;
    }

    /**
     * Get chip
     *
     * @return string 
     */
    public function getChip()
    {
        return $this->chip;
    }

    /**
     * Set vehiculo
     *
     * @param Vehiculo $vehiculo
     */
    public function setVehiculo(Vehiculo $vehiculo)
    {
        $this->vehiculo = $vehiculo;
    }

    /**
     * Get vehiculo
     *
     * @return Vehiculo
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }

    /**
     * Set tramas
     *
     * @param ArrayCollection $tramas
     */
    public function setTramas($tramas)
    {
        $this->tramas = $tramas;
    }

    /**
     * Get tramas
     *
     * @return ArrayCollection
     */
    public function getTramas()
    {
        return $this->tramas;
    }

    public function hasTrama($trama)
    {
        return $this->getTramas()->contains($trama);
    }

    public function addTrama(Trama $trama)
    {
        if (!$this->getTramas()->contains($trama)) {
            $this->getTramas()->add($trama);
        }

        return $this;
    }

    public function removeTrama(Trama $trama)
    {
        if ($this->getTramas()->contains($trama)) {
            $this->getTramas()->removeElement($trama);
        }

        return $this;
    }

    /**
     * Set cliente
     *
     * @param Cliente $cliente
     * @return Tracker
     */
    public function setCliente(Cliente $cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }
}
