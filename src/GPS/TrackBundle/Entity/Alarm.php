<?php

namespace GPS\TrackBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use GPS\UserBundle\Entity\Grupo;

/**
 * Alarm
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Alarm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Vehiculo", inversedBy="vehicles")
     */
    private $vehicles;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="GPS\UserBundle\Entity\Grupo", mappedBy="groups")
     */
    private $groups;

    /**
     * @var array
     *
     * @ORM\Column(name="signals", type="array")
     */
    private $signals;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="smallint")
     */
    private $level;

    /**
     * @var Cliente
     *
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id", nullable=false)
     */
    private $cliente;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Alarm
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Alarm
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set vehicles
     *
     * @param ArrayCollection $vehicles
     * @return Grupo
     */
    public function setVehicles(ArrayCollection $vehicles)
    {
        $this->vehicles = $vehicles;

        return $this;
    }

    /**
     * Get vehicles
     *
     * @return ArrayCollection
     */
    public function getVehicles()
    {
        return $this->vehicles ?: $this->vehicles = new ArrayCollection();
    }

    public function getVehiclePlacas()
    {
        $placas = array();
        foreach ($this->getVehicles() as $vehicle) {
            $placas[] = $vehicle->getPlaca();
        }

        return $placas;
    }

    public function hasVehicle($placa)
    {
        return in_array($placa, $this->getVehiclesPlacas());
    }

    public function addVehicle(Vehiculo $vehicle)
    {
        if (!$this->getVehicles()->contains($vehicle)) {
            $this->getVehicles()->add($vehicle);
        }

        return $this;
    }

    public function removeVehicle(Vehiculo $vehicle)
    {
        if ($this->getVehicles()->contains($vehicle)) {
            $this->getVehicles()->removeElement($vehicle);
        }

        return $this;
    }

    /**
     * Set groups
     *
     * @param ArrayCollection $groups
     * @return Grupo
     */
    public function setGroups(ArrayCollection $groups)
    {
        $this->groups = $groups;

        return $this;
    }

    /**
     * Gets the groups granted to the user.
     *
     * @return ArrayCollection
     */
    public function getGroups()
    {
        return $this->groups ?: $this->groups = new ArrayCollection();
    }

    public function getGroupNames()
    {
        $names = array();
        foreach ($this->getGroups() as $group) {
            $names[] = $group->getName();
        }

        return $names;
    }

    public function hasGroup($name)
    {
        return in_array($name, $this->getGroupNames());
    }

    public function addGroup(Grupo $group)
    {
        if (!$this->getGroups()->contains($group)) {
            $this->getGroups()->add($group);
        }

        return $this;
    }

    public function removeGroup(Grupo $group)
    {
        if ($this->getGroups()->contains($group)) {
            $this->getGroups()->removeElement($group);
        }

        return $this;
    }

    /**
     * Set signals
     *
     * @param array $signals
     * @return Alarm
     */
    public function setSignals($signals)
    {
        $this->signals = $signals;
    
        return $this;
    }

    /**
     * Get signals
     *
     * @return array 
     */
    public function getSignals()
    {
        return $this->signals;
    }

    /**
     * Set level
     *
     * @param integer $nivel
     * @return Alarm
     */
    public function setLevel($nivel)
    {
        $this->level = $nivel;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set cliente
     *
     * @param Cliente $cliente
     * @return Usuario
     */
    public function setCliente(Cliente $cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }

}
