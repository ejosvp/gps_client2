<?php

namespace GPS\TrackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Chofer
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Chofer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=255)
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="licencia", type="string", length=255)
     */
    private $licencia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="licencia_expira", type="date")
     */
    private $licenciaExpira;

    /**
     * @var string
     *
     * @ORM\Column(name="licencia_tipo", type="string", length=255)
     */
    private $licenciaTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", length=255)
     */
    private $dni;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_sangre", type="string", length=255, nullable=true)
     */
    private $tipoSangre;

    /**
     * @var string
     *
     * @ORM\Column(name="foto", type="string", length=255, nullable=true)
     */
    private $foto;

    /**
     * @var Cliente
     *
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id", nullable=false)
     */
    private $cliente;


    public function __toString()
    {
        return (string) $this->getApellido() . ", " . $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Chofer
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Chofer
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;
    
        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set licencia
     *
     * @param string $licencia
     * @return Chofer
     */
    public function setLicencia($licencia)
    {
        $this->licencia = $licencia;
    
        return $this;
    }

    /**
     * Get licencia
     *
     * @return string 
     */
    public function getLicencia()
    {
        return $this->licencia;
    }

    /**
     * Set licenciaExpira
     *
     * @param \DateTime $licenciaExpira
     * @return Chofer
     */
    public function setLicenciaExpira($licenciaExpira)
    {
        $this->licenciaExpira = $licenciaExpira;
    
        return $this;
    }

    /**
     * Get licenciaExpira
     *
     * @return \DateTime 
     */
    public function getLicenciaExpira()
    {
        return $this->licenciaExpira;
    }

    /**
     * Set licenciaTipo
     *
     * @param string $licenciaTipo
     * @return Chofer
     */
    public function setLicenciaTipo($licenciaTipo)
    {
        $this->licenciaTipo = $licenciaTipo;
    
        return $this;
    }

    /**
     * Get licenciaTipo
     *
     * @return string 
     */
    public function getLicenciaTipo()
    {
        return $this->licenciaTipo;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return Chofer
     */
    public function setDni($dni)
    {
        $this->dni = $dni;
    
        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set tipoSangre
     *
     * @param string $tipoSangre
     * @return Chofer
     */
    public function setTipoSangre($tipoSangre)
    {
        $this->tipoSangre = $tipoSangre;
    
        return $this;
    }

    /**
     * Get tipoSangre
     *
     * @return string 
     */
    public function getTipoSangre()
    {
        return $this->tipoSangre;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return Chofer
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    
        return $this;
    }

    /**
     * Get foto
     *
     * @return string 
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set cliente
     *
     * @param Cliente $cliente
     * @return Chofer
     */
    public function setCliente(Cliente $cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }
}
