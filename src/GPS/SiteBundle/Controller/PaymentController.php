<?php

namespace GPS\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Payment controller.
 *
 * @Route("/")
 */
class PaymentController extends Controller
{

    /**
     * Lists all Choferes.
     *
     * @Route("/disabled", name="gps_track_disabled")
     * @Method("GET")
     * @Template()
     */
    public function disabledAction()
    {
        return array();
    }

    /**
     * @param $action
     * @param $value
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->set(
            $action,
            $this->container->get('translator')->trans($value, [], 'GPSTrackBundle')
        );
    }
}
