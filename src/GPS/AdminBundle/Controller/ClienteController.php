<?php

namespace GPS\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GPS\TrackBundle\Entity\Cliente;
use GPS\AdminBundle\Form\ClienteType;

/**
 * Cliente controller.
 *
 * @Route("/cliente")
 */
class ClienteController extends Controller
{

    /**
     * Lists all Clientes.
     *
     * @Route("/", name="cliente")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $clientes = $this->get('gps_track.cliente_manager')->findAll();

        return array(
            'clientes' => $clientes,
        );
    }
    /**
     * Creates a new Cliente.
     *
     * @Route("/", name="cliente_create")
     * @Method("POST")
     * @Template("GPSTrackBundle:Cliente:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $cliente = $this->get('gps_track.cliente_manager')->create();
        $form = $this->createCreateForm($cliente);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('gps_track.cliente_manager')->update($cliente);

            $this->setFlash('success', 'cliente.flash.created');

            return $this->redirect($this->generateUrl('cliente'));
        }

        return array(
            'cliente' => $cliente,
            'form' => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Cliente.
    *
    * @param Cliente $cliente The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Cliente $cliente)
    {
        $form = $this->createForm(new ClienteType(), $cliente, array(
            'action' => $this->generateUrl('cliente_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Crear',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Cliente.
     *
     * @Route("/new", name="cliente_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $cliente = $this->get('gps_track.cliente_manager')->create();
        $form   = $this->createCreateForm($cliente);

        return array(
            'cliente' => $cliente,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Cliente.
     *
     * @Route("/{id}/edit", name="cliente_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $cliente = $this->get('gps_track.cliente_manager')->find($id);

        if (!$cliente) {
            throw $this->createNotFoundException('Unable to find Cliente.');
        }

        $form = $this->createEditForm($cliente);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'cliente' => $cliente,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Cliente.
    *
    * @param Cliente $cliente The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Cliente $cliente)
    {
        $form = $this->createForm(new ClienteType(), $cliente, array(
            'action' => $this->generateUrl('cliente_update', array('id' => $cliente->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn-primary')
        ));

        return $form;
    }
    /**
     * Edits an existing Cliente.
     *
     * @Route("/{id}", name="cliente_update")
     * @Method("PUT")
     * @Template("GPSTrackBundle:Cliente:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $cliente = $this->get('gps_track.cliente_manager')->find($id);

        if (!$cliente) {
            throw $this->createNotFoundException('Unable to find Cliente.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $form = $this->createEditForm($cliente);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('gps_track.cliente_manager')->update($cliente);

            $this->setFlash('success', 'cliente.flash.updated');

            return $this->redirect($this->generateUrl('cliente'));
        }

        return array(
            'cliente' => $cliente,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Cliente.
     *
     * @Route("/{id}", name="cliente_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $cliente = $this->get('gps_track.cliente_manager')->find($id);

            if (!$cliente) {
                throw $this->createNotFoundException('Unable to find Cliente.');
            }

            $this->get('gps_track.cliente_manager')->delete($cliente);
        }

        return $this->redirect($this->generateUrl('cliente'));
    }

    /**
     * Creates a form to delete a Cliente by id.
     *
     * @param mixed $id The cliente id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cliente_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Eliminar',
                'attr' => array('class' => 'btn-danger')
            ))
            ->getForm()
        ;
    }

    /**
     * Enable/Disable a Client.
     *
     * @Route("/{id}/enable", name="cliente_enable")
     * @Method("GET")
     */
    public function enableAction($id)
    {
        $cliente = $this->get('gps_track.cliente_manager')->find($id);

        if (!$cliente)
        {
            throw $this->createNotFoundException('Unable to find Cliente.');
        }

        $cliente->setActivo(!$cliente->getActivo());
        $this->get('gps_track.cliente_manager')->update($cliente);

        $this->setFlash('success', 'cliente.flash.' . ($cliente->getActivo() ? 'enabled' : 'disabled'));

        return $this->redirect($this->generateUrl('cliente'));
    }

    /**
     * @param $action
     * @param $value
     */
    protected function setFlash($action, $value)
    {
        $this->get('session')->getFlashBag()->set(
            $action,
            $this->container->get('translator')->trans($value, [], 'GPSAdminBundle')
        );
    }
}
